/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.scheduler;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;
import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.connector.Connector;

public class DiscreteScheduler extends AbsScheduler {
	private static Path runtimePath = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");

	public DiscreteScheduler(Connector conn, Deployer depl) {
		this.conn = conn;
		this.depl = depl;
	}

	@Override
	boolean performScheduling(Resource designTimeModel, Resource runtimeModel) throws ArchitectureSchedulingException {
		LOGGER.info("Deriving Required Runtime model");
		CachedResourceSet.getCache().clear();

		org.eclipse.emf.ecore.resource.Resource reqRuntimeModel = getRequiredConfiguration(designTimeModel,
				runtimeModel);
		LOGGER.info("Deploying Runtime Model");

		Comparator comparator = ComparatorFactory.getComparator("Simple", reqRuntimeModel, runtimeModel);
		if (comparator.getMissingElements().isEmpty() && comparator.getNewElements().isEmpty()) {
			LOGGER.info("No new or missing elements detected. Skipping deployment.");
			return false;
		} else {
			performDeploy(reqRuntimeModel);
		}

		try {
			reqRuntimeModel.setURI(URI.createFileURI(currentJobHistoryPath + "/ReqRun.occic"));
			reqRuntimeModel.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			System.out.println("Could not store reqruntime model");
			e.printStackTrace();
		}

		return true;
	}
}
