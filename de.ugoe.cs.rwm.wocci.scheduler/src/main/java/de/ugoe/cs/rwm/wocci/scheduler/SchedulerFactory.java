package de.ugoe.cs.rwm.wocci.scheduler;

import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.connector.Connector;

public class SchedulerFactory {
	public static ArchitectureScheduler getArchScheduler(String criteria, Connector conn, Deployer depl) {
		if (criteria.equals("Discrete")) {
			return new DiscreteScheduler(conn, depl);
		}
		if (criteria.equals("Batch")) {
			return new BatchScheduler(conn, depl);
		}
		throw new IllegalArgumentException("ArchitectureScheduler: " + criteria + " does not exist."
				+ "Supported Schedulers:" + " Batch, Discrete");
	}
}
