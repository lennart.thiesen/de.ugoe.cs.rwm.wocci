/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.scheduler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;

import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.wocci.scheduler.history.SchedulerHistory;
import de.ugoe.cs.rwm.wocci.scheduler.transformation.DESIGN2REQRUNTIMETransformator;

public abstract class AbsScheduler extends Observable implements ArchitectureScheduler {
	protected Connector conn;
	protected Deployer depl;
	protected String jobHistoryPath;
	protected String currentJobHistoryPath;
	protected List<Observer> observers = new ArrayList<>();
	protected static Path reqRuntimePath = Paths.get(System.getProperty("user.home") + "/.rwm/reqRuntime.occic");
	private long transTimeStart;
	private long transTimeEnd;
	private long engineTimeStart;
	private long engineTimeEnd;

	abstract boolean performScheduling(Resource designTimeModel, Resource runtimeModel)
			throws ArchitectureSchedulingException;

	protected void performDeploy(Resource reqRuntimeModel) throws ArchitectureSchedulingException {
		engineTimeStart = System.currentTimeMillis();
		depl.deploy(reqRuntimeModel);
		engineTimeEnd = System.currentTimeMillis();
	}

	protected org.eclipse.emf.ecore.resource.Resource getRequiredConfiguration(
			org.eclipse.emf.ecore.resource.Resource designtimeModel,
			org.eclipse.emf.ecore.resource.Resource runtimeModel) throws ArchitectureSchedulingException {
		transTimeStart = System.currentTimeMillis();
		Transformator trans = new DESIGN2REQRUNTIMETransformator();
		org.eclipse.emf.ecore.resource.Resource reqRuntimeModel;
		try {
			trans.transform(designtimeModel, runtimeModel, reqRuntimePath);
			LOGGER.info("TRANSFORMATION FINISHED!");
			CachedResourceSet.getCache().clear();
			reqRuntimeModel = de.ugoe.cs.rwm.docci.ModelUtility.loadOCCIintoEMFResource(reqRuntimePath);
			LOGGER.info("Config loaded into resource!");
		} catch (RuntimeException | EolRuntimeException e) {
			throw new ArchitectureSchedulingException("Design2Reqruntime transformation failed.", e);
		}
		transTimeEnd = System.currentTimeMillis();
		return reqRuntimeModel;

	}

	@Override
	public void scheduleArchitecture(Resource designTimeModel, Resource runtimeModel)
			throws ArchitectureSchedulingException {
		long startDate = System.currentTimeMillis();
		File histDir = createSchedulingFolder(new Date(startDate));
		this.depl.setJobHistoryPath(currentJobHistoryPath + "/");
		// If scheduling is required log everything
		// Within performScheduling it is checked whether the deployment process needs
		// to be triggered
		if (performScheduling(designTimeModel, runtimeModel)) {
			try {
				createSchedulerHistory(histDir, designTimeModel, runtimeModel, startDate, System.currentTimeMillis(),
						transTimeStart, transTimeEnd, engineTimeStart, engineTimeEnd);
			} catch (IOException e) {
				e.printStackTrace();
			}
			for (Observer o : observers) {
				o.update(this, this);
			}
		}
	}

	private File createSchedulingFolder(Date startDate) {
		// SimpleDateFormat dateFormat = new SimpleDateFormat("hh_mm_ss");
		// String time = dateFormat.format(startDate);
		Long time = new Date().getTime();
		File dir = new File(jobHistoryPath + time);
		currentJobHistoryPath = jobHistoryPath + time;
		dir.mkdir();
		return dir;
	}

	public void addObserver(Observer o) {
		this.observers.add(o);
	}

	public Connector getConnector() {
		return conn;
	}

	public void setJobHistoryPath(String jobHistoryPath) {
		this.jobHistoryPath = jobHistoryPath;
	}

	public String getJobHistoryPath() {
		return this.jobHistoryPath;
	}

	private void createSchedulerHistory(File dir, Resource designTimeModel, Resource runtimeModel, long startDate,
			long endDate, long transTimeStart, long transTimeEnd, long engineTimeStart, long engineTimeEnd)
			throws IOException {
		designTimeModel.setURI(URI.createFileURI(dir.toString() + "/Design.occic"));
		designTimeModel.save(Collections.EMPTY_MAP);
		runtimeModel.setURI(URI.createFileURI(dir.toString() + "/Run.occic"));
		runtimeModel.save(Collections.EMPTY_MAP);
		// reqRuntimeModel.setURI(URI.createFileURI(dir.toString() + "/ReqRun.occic"));
		// reqRuntimeModel.save(Collections.EMPTY_MAP);
		SchedulerHistory hist = new SchedulerHistory(startDate, endDate, transTimeStart, transTimeEnd, engineTimeStart,
				engineTimeEnd);
		hist.store(dir.getAbsolutePath());
	}
}
