/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.scheduler;

public class ArchitectureSchedulingException extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ArchitectureSchedulingException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}
}
