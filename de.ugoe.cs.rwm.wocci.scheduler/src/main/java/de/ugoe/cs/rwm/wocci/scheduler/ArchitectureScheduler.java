/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.scheduler;

import de.ugoe.cs.rwm.docci.connector.Connector;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.resource.Resource;

import java.io.IOException;

public interface ArchitectureScheduler {
	static Logger LOGGER = Logger.getLogger(ArchitectureScheduler.class.getName());

	public void scheduleArchitecture(Resource designTimeModel, Resource runtimeModel)
			throws ArchitectureSchedulingException;

	public Connector getConnector();

	public void setJobHistoryPath(String jobHistoryPath);

	public String getJobHistoryPath();
}
