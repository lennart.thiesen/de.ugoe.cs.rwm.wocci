package de.ugoe.cs.rwm.wocci.scheduler.history;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.ugoe.cs.rwm.docci.history.DocciHistory;

public class SchedulerHistory {
	private long startDate;
	private Date readableStartDate;
	private long endDate;
	private Date readableEndDate;
	private long duration;
	private long transTimeStart;
	private long transTimeEnd;
	private long transDuration;
	private long engineStart;
	private long engineEnd;
	private long engineDuration;
	private long other;

	public SchedulerHistory(long startDate, long endDate, long transTimeStart, long transTimeEnd, long engineStart,
			long engineEnd) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.readableStartDate = new Date(startDate);
		this.readableEndDate = new Date(endDate);
		this.transTimeStart = transTimeStart;
		this.transTimeEnd = transTimeEnd;
		this.engineStart = engineStart;
		this.engineEnd = engineEnd;

		long durationInMilliSeconds = Math.abs(engineEnd - engineStart);
		engineDuration = TimeUnit.MILLISECONDS.convert(durationInMilliSeconds, TimeUnit.MILLISECONDS);

		durationInMilliSeconds = Math.abs(transTimeEnd - transTimeStart);
		transDuration = TimeUnit.MILLISECONDS.convert(durationInMilliSeconds, TimeUnit.MILLISECONDS);

		durationInMilliSeconds = Math.abs(endDate - startDate);
		this.duration = TimeUnit.MILLISECONDS.convert(durationInMilliSeconds, TimeUnit.MILLISECONDS);

		this.other = duration - engineDuration - transDuration;
	}

	public SchedulerHistory() {
	}

	public void store(String jobHistoryPath) {
		if (jobHistoryPath != null && jobHistoryPath.equals("") == false) {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				File logDir = new File(jobHistoryPath);
				if (!logDir.exists()) {
					System.out.println("Creating log Directory.");
					logDir.mkdirs();
				}
				try {
					extractEngineValues(jobHistoryPath);
				} catch (IOException e) {
					System.out.println("Could not overwrite metrics with exact values from DOCCI engine. "
							+ "Sticking to the ones gathered by WOCCI.");
				}
				objectMapper.writeValue(new File(jobHistoryPath + "/scheduler.json"), this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("No job history path set. Can not store task history!");
		}
	}

	private void extractEngineValues(String jobHistoryPath) throws IOException {
		for (File file : new File(jobHistoryPath).listFiles()) {
			if (file.isDirectory() && file.getName().startsWith("job")) {
				for (File cFile : file.listFiles()) {
					if (cFile.getName().startsWith("job")) {
						DocciHistory dHist = (DocciHistory) extractHistory(cFile, DocciHistory.class);
						engineStart = dHist.getStartDate();
						engineEnd = dHist.getEndDate();
						engineDuration = dHist.getDuration();
						this.other = duration - engineDuration - transDuration;
					}
				}
			}
		}
	}

	private Object extractHistory(File jobJson, Class clazz) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Object hist = objectMapper.readValue(jobJson, clazz);
		return hist;
	}

	public long getStartDate() {
		return startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public long getDuration() {
		return duration;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public Date getReadableStartDate() {
		return readableStartDate;
	}

	public void setReadableStartDate(Date readableStartDate) {
		this.readableStartDate = readableStartDate;
	}

	public Date getReadableEndDate() {
		return readableEndDate;
	}

	public void setReadableEndDate(Date readableEndDate) {
		this.readableEndDate = readableEndDate;
	}

	public long getTransTimeStart() {
		return transTimeStart;
	}

	public void setTransTimeStart(long transTimeStart) {
		this.transTimeStart = transTimeStart;
	}

	public long getTransTimeEnd() {
		return transTimeEnd;
	}

	public void setTransTimeEnd(long transTimeEnd) {
		this.transTimeEnd = transTimeEnd;
	}

	public long getTransDuration() {
		return transDuration;
	}

	public void setTransDuration(long transDuration) {
		this.transDuration = transDuration;
	}

	public long getEngineStart() {
		return engineStart;
	}

	public void setEngineStart(long engineStart) {
		this.engineStart = engineStart;
	}

	public long getEngineEnd() {
		return engineEnd;
	}

	public void setEngineEnd(long engineEnd) {
		this.engineEnd = engineEnd;
	}

	public long getEngineDuration() {
		return engineDuration;
	}

	public void setEngineDuration(long engineDuration) {
		this.engineDuration = engineDuration;
	}

	public long getOther() {
		return other;
	}

	public void setOther(long other) {
		this.other = other;
	}
}
