/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.scheduler.transformation.test;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.wocci.scheduler.transformation.DESIGN2REQRUNTIMETransformator;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class Design2ReqRuntimeMLSDecision2Test {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestExtensionRegistry.extensionRegistrySetup();
		TestUtility.loggerSetup();
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void MLS_Start() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mlsDecision2/MLS_Decision_Manual2.occic"));
		Path runOcci = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/mlsDecision2/mlsDecision2CoarseActive.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);
	}

	@Test
	public void Decision_Finished() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/mlsDecision2/MLS_Decision_Manual2.occic"));
		Path runOcci = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/mlsDecision2/mlsDecision2DecFinished.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);
	}
}
