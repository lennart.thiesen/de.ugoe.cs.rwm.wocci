/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.scheduler.transformation.test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OCCIResourceFactoryImpl;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.deprovisioner.Deprovisioner;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.extractor.Extractor;
import de.ugoe.cs.rwm.docci.provisioner.Provisioner;
import de.ugoe.cs.rwm.tocci.Transformator;

public class TestUtility {

	public static void loggerSetup() {
		File log4jfile = new File(ModelUtility.getPathToResource("log4j.properties"));
		PropertyConfigurator.configure(log4jfile.getAbsolutePath());
		Logger.getLogger(Executor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Extractor.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Provisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Deprovisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Connector.class.getName()).setLevel(Level.OFF);
		// Logger.getLogger(ElementAdapter.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Deployer.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.OFF);
	}

	public static Configuration loadConfiguration(Connector conn) {
		Path deployedOCCI = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
		conn.loadRuntimeModel(deployedOCCI);

		Configuration config = ModelUtility.loadOCCIConfiguration(deployedOCCI);
		return config;
	}

	public static String getPathToResource(String resourceName) {
		try {
			return ClassLoader.getSystemClassLoader().getResource(resourceName).getFile();
		} catch (NullPointerException e) {
			// fail("Resource "+resourceName+" could not be found in resource folder!");
			return null;
		}
	}

	public static Resource loadOCCIResource(Path configuration) {
		OCCIPackage.eINSTANCE.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("occie", new OCCIResourceFactoryImpl());
		m.put("occic", new OCCIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		String file = new File(configuration.toString()).getAbsolutePath();

		Resource resource = resSet.getResource(URI.createFileURI(file), true);

		EcorePlugin.ExtensionProcessor.process(null);
		EcoreUtil.resolveAll(resSet);

		return resource;
	}

	public static int numberOfVms(Configuration config) {
		int count = 0;
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res instanceof Compute) {
				count++;
			}
		}
		return count;
	}

	public static boolean detectDuplicateLinks(Configuration config) {
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			List<org.eclipse.cmf.occi.core.Resource> targets = new ArrayList<org.eclipse.cmf.occi.core.Resource>();
			for (Link link : res.getLinks()) {
				if (targets.contains(link.getTarget())) {
					System.out.println("Duplicated Link: " + link);
					return true;
				}
				targets.add((org.eclipse.cmf.occi.core.Resource) link.getTarget());
			}
		}
		return false;
	}
}
