/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.connector;

import org.occiware.mart.server.model.ConfigurationManager;
import org.occiware.mart.server.model.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Slave objects
 *
 * @author erbel
 *
 */
public class PostConnectorConstructor implements Runnable {
	private static final long ONE_SECOND = 1000;
	static Logger LOGGER = LoggerFactory.getLogger(TaskConnector.class);
	private int timeout;

	public PostConnectorConstructor(int timeout) {
		this.timeout = timeout;
	}

	@Override
	public void run() {
		int count = 0;
		while(count<timeout){
			LOGGER.info("Waiting for eContainer to be loaded!");
			try {
				Thread.sleep(ONE_SECOND);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			count++;
		}
		LOGGER.info("Finished Waiting!");
		addElement();

	}

	private void addElement(){
		String uuid = ConfigurationManager.createUUID();
		String summary = "sum";
		String kind = "http://schemas.ugoe.cs.rwm/workflow#task";

		List<String> mixins = new ArrayList<>();
		Map<String, String> attributes = new HashMap<>();
		String location = "/task/urn:uuid:"+uuid+"/";
		String owner = "anonymous";
		attributes.put("occi.core.title", "bla");

		try {
			EntityManager.addResourceToConfiguration(uuid,
					"test", summary, kind, mixins,attributes,location,owner);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
