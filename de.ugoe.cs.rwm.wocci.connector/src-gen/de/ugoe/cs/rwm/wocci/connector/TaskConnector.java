/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Tue Aug 28 15:09:33 CEST 2018 from platform:/resource/workflow/model/workflow.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.wocci.connector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.core.OCCIFactory;
import org.modmacao.occi.platform.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import workflow.Datalink;
import workflow.Executionlink;
import workflow.Loopiteration;
import workflow.Status;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/workflow# - term: task - title: Task
 */
public class TaskConnector extends workflow.impl.TaskImpl {
	/**
	 * Initialize the logger.
	 */
	private static Logger LOGGER = LoggerFactory.getLogger(TaskConnector.class);
	// protected List<LoopConnector> observers = new ArrayList<>();
	private Set<LoopConnector> observers = new HashSet<LoopConnector>();

	// Start of user code Taskconnector_constructor
	/**
	 * Constructs a task connector.
	 */
	TaskConnector() {
		LOGGER.debug("Constructor called on " + this);
	}
	// End of user code
	//
	// OCCI CRUD callback operations.
	//

	// Start of user code TaskocciCreate
	/**
	 * Called when this Task instance is completely created.
	 */
	@Override
	public void occiCreate() {
		LOGGER.debug("occiCreate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Task_occiRetrieve_method
	/**
	 * Called when this Task instance must be retrieved.
	 */
	@Override
	public void occiRetrieve() {
		LOGGER.debug("occiRetrieve() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Task_occiUpdate_method
	/**
	 * Called when this Task instance is completely updated.
	 */
	@Override
	public void occiUpdate() {
		LOGGER.debug("occiUpdate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code TaskocciDelete_method
	/**
	 * Called when this Task instance will be deleted.
	 */
	@Override
	public void occiDelete() {
		LOGGER.debug("occiDelete() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	//
	// Task actions.
	//

	// Start of user code Task_Kind_schedule_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: schedule - title:
	 * schedule
	 */
	@Override
	public void schedule() {
		LOGGER.debug("Action schedule() called on " + this);

		// Task State Machine.
		switch (getWorkflowTaskState().getValue()) {
		case Status.ERROR_VALUE:
			LOGGER.debug("Fire transition(state=error, action=\"schedule\")...");
			scheduleExecutables();
			setState(Status.SCHEDULED);
			setWorkflowTaskStateMessage("Rescheduled");
			break;
		case Status.FINISHED_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"schedule\")...");
			scheduleExecutables();
			setWorkflowTaskStateMessage("Rescheduled");
			setState(Status.SCHEDULED);
			break;
		case Status.SKIPPED_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"schedule\")...");
			// TODO Implement transition(state=inactive, action="schedule")
			setState(Status.SCHEDULED);
			setWorkflowTaskStateMessage("Rescheduled");
			break;
		default:
			break;
		}
	}

	private void scheduleExecutables() {
		for (Executionlink execL : ModelUtility.getSpecificLinks(this, Executionlink.class)) {
			if (execL.getTarget() instanceof Component) {
				((Component) execL.getTarget()).undeploy();
			}
		}

	}

	// End of user code
	// Start of user code Task_Kind_stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: stop - title:
	 */
	@Override
	public void stop() {
		LOGGER.debug("Action stop() called on " + this);

		// Task State Machine.
		switch (getWorkflowTaskState().getValue()) {

		case Status.ACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=active, action=\"stop\")...");
			// TODO Implement transition(state=active, action="stop")
			setState(Status.INACTIVE);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Task_Kind_stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: skip - title:
	 */
	@Override
	public void skip() {
		LOGGER.debug("Action stop() called on " + this);

		// Task State Machine.
		switch (getWorkflowTaskState().getValue()) {

		case Status.SCHEDULED_VALUE:
			LOGGER.debug("Fire transition(state=scheduled, action=\"skip\")...");
			setState(Status.SKIPPED);
			break;

		case Status.INACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"skip\")...");
			setState(Status.SKIPPED);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Task_Kind_start_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: start - title: start
	 */
	@Override
	public void start() {
		LOGGER.debug("Action start() called on " + this);
		// Task State Machine.
		switch (getWorkflowTaskState().getValue()) {

		case Status.SCHEDULED_VALUE:
			LOGGER.debug("Fire transition(state=scheduled, action=\"start\")...");
			this.setState(Status.ACTIVE);
			perform("start");
			if (checkCompsForState(org.modmacao.occi.platform.Status.ACTIVE.getValue())) {
				this.setState(Status.FINISHED);
			} else {
				this.setState(Status.ERROR);
			}

			break;

		case Status.INACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"start\")...");
			this.setState(Status.ACTIVE);
			perform("start");
			if (checkCompsForState(org.modmacao.occi.platform.Status.ACTIVE.getValue())) {
				this.setState(Status.FINISHED);
			} else {
				this.setState(Status.ERROR);
			}
			setState(Status.ACTIVE);
			break;

		default:
			break;
		}
	}

	private void perform(String string) {
		if (this.getExecutionlinkComponents().isEmpty() == false) {
			if (string.equals("start")) {
				updateAttributesOfExecutables();
				List<RunnableExeclink> slaves = createSubtasks(this.getExecutionlinkComponents(), "start");
				List<Thread> threads = new ArrayList<Thread>();

				for (RunnableExeclink slave : slaves) {
					Thread thread = new Thread(slave);
					threads.add(thread);
					thread.start();
				}
				for (Thread t : threads) {
					try {
						t.join();
						LOGGER.info("Thread: " + t + "joined");
					} catch (InterruptedException e) { // TODO Auto-generated catch
						e.printStackTrace();
					}
					LOGGER.info("Threads joined: Emptying blocked list");
				}
			}
		}
	}

	private void updateAttributesOfExecutables() {
		// DA KOMMTERNICHREIN
		for (MixinBase mixB : this.getParts()) {
			if (mixB instanceof Loopiteration) {
				ModelUtility.updateTaskExecutableWithLoopInformation(this, (Loopiteration) mixB);
			}
		}
		// This part is a workaround and should be implemented as a new Mixin
		for (Link link : this.getLinks()) {
			if (link instanceof Datalink) {
				System.out.println("Datalink detected!");
				Datalink dl = (Datalink) link;
				injectDataLinkInformation(dl);
			}
		}
		// This part is a workaround and should be implemented as a new Mixin
		for (Link rlink : this.getRlinks()) {
			if (rlink instanceof Datalink) {
				System.out.println("R - Datalink detected!");
				Datalink dl = (Datalink) rlink;
				injectRDataLinkInformation(dl);
			}
		}
	}

	private void injectDataLinkInformation(Datalink dl) {
		for (Component comp : this.getExecutionlinkComponents()) {
			System.out.println("Injecting datalink information in: " + comp.getTitle());
			AttributeState attar = OCCIFactory.eINSTANCE.createAttributeState();
			attar.setName("task.output");
			attar.setValue(dl.getTaskDatalinkSourceFile());
			comp.getAttributes().add(attar);
			comp.getAttributes();
		}
	}

	private void injectRDataLinkInformation(Datalink dl) {
		for (Component comp : this.getExecutionlinkComponents()) {
			System.out.println("Injecting R datalink information in: " + comp.getTitle());
			AttributeState attar = OCCIFactory.eINSTANCE.createAttributeState();
			attar.setName("task.input");
			attar.setValue(dl.getTaskDatalinkTargetFile());
			comp.getAttributes().add(attar);
			comp.getAttributes();
		}
	}

	private List<Component> getExecutionlinkComponents() {
		List<Component> comps = new ArrayList<Component>();
		for (Executionlink link : ModelUtility.getSpecificLinks(this, Executionlink.class)) {
			if (link.getTarget() != null) {
				if (link.getTarget() instanceof Component) {
					comps.add((Component) link.getTarget());
				}
			}
		}
		return comps;
	}

	// End of user code
	private List<RunnableExeclink> createSubtasks(List<Component> comps, String action) {
		List<RunnableExeclink> slaves = new ArrayList<RunnableExeclink>();

		for (Component comp : comps) {
			RunnableExeclink slave = new RunnableExeclink(this, comp, action);
			slaves.add(slave);
		}
		return slaves;
	}

	private boolean checkCompsForState(int state) {
		boolean success = true;
		for (Component comp : this.getExecutionlinkComponents()) {
			if (!(comp.getOcciComponentState().getValue() == state)) {
				success = false;
			}
		}
		return success;
	}
	// End of user code

	public void attachObserver(LoopConnector obs) {
		this.observers.add(obs);
	}

	public void dettachObserver(LoopConnector obs) {
		this.observers.remove(obs);
	}

	public void emptyObserverList() {
		this.observers.clear();
	}

	public Status getState() {
		return this.getWorkflowTaskState();
	}

	public void setState(Status status) {
		this.setWorkflowTaskState(status);
		if (status.equals(Status.FINISHED) || status.equals(Status.SKIPPED)) {
			notifyObserver();
		}
	}

	private void notifyObserver() {
		LOGGER.info("Notifying Loop Observer of Task: " + this.getId());
		for (LoopConnector observer : observers) {
			LOGGER.info("      " + observer);
			observer.update();
		}
	}
}
