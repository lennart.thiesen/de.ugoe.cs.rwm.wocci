package de.ugoe.cs.rwm.wocci.connector.decision.processor.loop;

import java.util.List;

import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.MixinBase;

import de.ugoe.cs.rwm.wocci.connector.LoopConnector;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.AbsProcessor;
import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import workflow.*;

public abstract class AbsLoopProcessor extends AbsProcessor {

	protected abstract Loopiteration createIterationVar();

	public AbsLoopProcessor(Decision decision) {
		super(decision);
	}

	@Override
	protected void performMixinSpecificTask(List<Controlflowlink> trueCLinks) {
		if (getParallelMixin() == null) {
			Loopiteration itVar = createIterationVar();
			for (Controlflowlink cLink : trueCLinks) {
				Task task = (Task) cLink.getTarget();
				annotateTaskSequence(task, itVar);
				rescheduleTaskSequence(task);
			}
		} else {
			for (Controlflowlink cLink : trueCLinks) {
				Task task = (Task) cLink.getTarget();
				rescheduleTaskSequence(task);
			}
		}

		LOGGER.info("Loop state(" + decision.getId() + "): All prev Tasks finished: " + allPreviousTasksFinished()
				+ " | All nested Task finished: " + allNestedTasksFinished());
		if (allPreviousTasksFinished() && allNestedTasksFinished()) {
			// decision.setWorkflowTaskState(Status.FINISHED);
			((LoopConnector) decision).setState(Status.FINISHED);
		} else {
			((LoopConnector) decision).setState(Status.ACTIVE);
		}
	}

	@Override
	public void processGatheredInformation() {
		if (informationIsGathered(decision) && allNestedTasksFinished()) {
			List<Controlflowlink> trueCLinks = evaluateControlFlowGuards();
			performMixinSpecificTask(trueCLinks);
		}
	}

	private Boolean allNestedTasksFinished() {
		boolean nestedTasksFinished = true;
		for (Nesteddependency nest : ModelUtility.getSpecificLinks(decision, Nesteddependency.class)) {
			if (nest.getTarget() != null) {
				Task task = (Task) nest.getTarget();
				if (task.getWorkflowTaskState().getValue() != Status.FINISHED_VALUE) {
					nestedTasksFinished = false;
				}
			}
		}
		return nestedTasksFinished;
	}

	private Boolean allPreviousTasksFinished() {
		boolean previousTasksFinished = true;
		for (Link link : decision.getRlinks()) {
			if (link instanceof Taskdependency) {
				Taskdependency tdep = (Taskdependency) link;
				Task srcTask = (Task) tdep.getSource();
				if (srcTask.getWorkflowTaskState().getValue() != Status.FINISHED_VALUE
						&& srcTask.getWorkflowTaskState().getValue() != Status.SKIPPED_VALUE) {
					previousTasksFinished = false;
				}
			}
		}
		return previousTasksFinished;
	}

	private Parallelloop getParallelMixin() {
		for (MixinBase mixB : decision.getParts()) {
			if (mixB instanceof Parallelloop) {
				return (Parallelloop) mixB;
			}
		}
		return null;
	}

	protected void annotateTaskSequence(Task task, Loopiteration itVar) {
		updateLoopIterationMixin(task, itVar);
		for (Taskdependency tDep : ModelUtility.getSpecificLinks(task, Taskdependency.class)) {
			Task tarTask = (Task) tDep.getTarget();
			if (task.getId().equals(decision.getId()) == false) {
				annotateTaskSequence(tarTask, itVar);
			}
		}

	}

	private void updateLoopIterationMixin(Task task, Loopiteration itVar) {
		if (itVar.getLoopIterationVarValue().equals("") == false) {
			MixinBase loopIteration = ModelUtility.getLoopIterationMixin(task);

			if (loopIteration == null) {
				ModelUtility.createLoopIterationMixin(task);
			}
			ModelUtility.updateLoopIterationInformation(task, itVar.getLoopIterationVarName(),
					itVar.getLoopIterationVarValue());
		}
	}

}
