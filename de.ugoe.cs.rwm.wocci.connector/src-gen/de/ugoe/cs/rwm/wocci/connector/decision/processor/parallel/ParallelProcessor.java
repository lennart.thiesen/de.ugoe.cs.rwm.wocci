package de.ugoe.cs.rwm.wocci.connector.decision.processor.parallel;

import java.util.*;

import org.eclipse.cmf.occi.core.*;
import org.eclipse.emf.common.util.BasicEList;
import org.occiware.mart.server.model.ConfigurationManager;
import org.occiware.mart.server.model.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

import de.ugoe.cs.rwm.wocci.connector.ConnectorFactory;
import de.ugoe.cs.rwm.wocci.connector.LoopConnector;
import de.ugoe.cs.rwm.wocci.connector.TaskConnector;
import workflow.*;

public class ParallelProcessor {
	private static Logger LOGGER = LoggerFactory.getLogger(ParallelProcessor.class);
	private LoopConnector loop;
	private int numberOfReplicas;
	private List<String> loopSplit;
	private List<TaskConnector> loopedTasks;

	public ParallelProcessor(LoopConnector loop, Parallelloop par) {
		this.loop = loop;
		this.numberOfReplicas = par.getParallelReplicateNumber();
		if (numberOfReplicas == 0) {
			LOGGER.warn("Invalid replica number detected (0)! Setting replica number to 1!");
			numberOfReplicas = 1;
		}
		this.loopSplit = calculateLoopSplit();
		this.numberOfReplicas = loopSplit.size();
		this.loopedTasks = this.loop.getLoopedTasks();
		this.loop.setWorkflowDecisionInput("");
	}

	public void performParallelization() {
		LOGGER.info("Performing parallelisation adjustments!");
		for (int i = 0; i < numberOfReplicas; i++) {
			createReplica(i);
		}

	}

	private void createReplica(int i) {

		HashMap<String, String> replicaMap = new HashMap<String, String>();
		LOGGER.info("Creating Loop Replica: " + i);
		String loopDuplicateLocation = duplicateResource(loop, null);
		String loopDuplicateId = loopDuplicateLocation.split("/")[2];
		LOGGER.info(loopDuplicateId);
		replicaMap.put(loop.getLocation(), loopDuplicateLocation);

		if (isLastReplica(i)) {
			LOGGER.info("Last replica detected: Task is not replicated! Reconnection is performed!");
			connectOriginalTaskToLoopDuplicate(loopDuplicateLocation);
			// LOGGER.info("Reconnecting platform dependencies!");
			// connectOriginalPlatformDeptoLoopDuplicate(loopDuplicateLocation);
		} else {
			for (Task task : loopedTasks) {
				LOGGER.info("Duplicating Task: " + task);
				String taskDuplicateLocation = duplicateResource(task, loopDuplicateId);
				replicaMap.put(task.getLocation(), taskDuplicateLocation);
				for (Link link : task.getRlinks()) {
					if (link.getSource().getId().equals(loop.getId())) {
						LOGGER.info("Connecting Loop to Task");
						String entryLinkLocation = duplicateLink(link, replicaMap);
						replicaMap.put(link.getLocation(), entryLinkLocation);
					}
				}
			}
			LOGGER.info("Duplicating Controlflow Between Tasks!");
			for (Task task : loopedTasks) {
				for (Link link : task.getLinks()) {
					if (link instanceof Taskdependency) {
						String cFlowLocation = duplicateLink(link, replicaMap);
						replicaMap.put(link.getLocation(), cFlowLocation);
					}
				}
			}
		}

		LOGGER.info("Connecting Loops!");
		linkLoopToLoop(loopDuplicateLocation, loop);
	}

	private void connectOriginalPlatformDeptoLoopDuplicate(String loopDuplicateLocation) {
		List<Link> toAdjust = new BasicEList<Link>();
		for (Link link : loop.getLinks()) {
			if (link instanceof Platformdependency) {
				toAdjust.add(link);
			}
		}
		for (Link link : toAdjust) {
			reconnectPlatformDependency(link, loopDuplicateLocation);
		}
	}

	private void reconnectPlatformDependency(Link link, String loopDuplicateLocation) {
		if (link != null) {
			LOGGER.info("Connecting Platformdependency to Loop");
			Map<String, String> attributes = getAttributeMap(link);
			attributes.put("occi.core.source", loopDuplicateLocation);
			String kind = link.getKind().getScheme() + link.getKind().getTerm();
			List<String> mixins = getMixinNames(link);
			String title = attributes.get("occi.core.title");
			String location = deriveLocation(link);
			String owner = getOwnerOfThisElement();

			String src = loopDuplicateLocation;
			String tar = link.getTarget().getLocation();
			try {
				EntityManager.addLinkToConfiguration(link.getId(), title, kind, mixins, src, tar, attributes, location,
						owner);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void connectOriginalTaskToLoopDuplicate(String loopDuplicateLocation) {
		for (TaskConnector task : loopedTasks) {
			Link entry = null;
			Link exit = null;
			task.emptyObserverList();
			for (Link link : task.getRlinks()) {
				if (link.getSource().getId().equals(loop.getId())) {
					entry = link;
				}
			}
			for (Link link : task.getLinks()) {
				if (link.getTarget().getId().equals(loop.getId())) {
					exit = link;
				}
			}
			reconnectEntryLinkToLoopDuplicate(entry, loopDuplicateLocation);
			reconnectExitLinkToLoopDuplicate(exit, loopDuplicateLocation);
		}
	}

	private boolean isLastReplica(int i) {
		if (i + 1 == numberOfReplicas) {
			return true;
		}
		return false;
	}

	private void reconnectEntryLinkToLoopDuplicate(Link link, String loopDuplicateLocation) {
		if (link != null) {
			LOGGER.info("Connecting Loop to Task");
			Map<String, String> attributes = getAttributeMap(link);
			attributes.put("occi.core.source", loopDuplicateLocation);
			String kind = link.getKind().getScheme() + link.getKind().getTerm();
			List<String> mixins = getMixinNames(link);
			String title = attributes.get("occi.core.title");
			String location = deriveLocation(link);
			String owner = getOwnerOfThisElement();

			String src = loopDuplicateLocation;
			String tar = link.getTarget().getLocation();
			try {
				EntityManager.addLinkToConfiguration(link.getId(), title, kind, mixins, src, tar, attributes, location,
						owner);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private String deriveLocation(Entity ent) {
		String location;
		if (ent.getId().startsWith("urn:uuid")) {
			location = "/" + ent.getKind().getTerm().toLowerCase() + "/" + ent.getId() + "/";
		} else {
			location = "/" + ent.getKind().getTerm().toLowerCase() + "/urn:uuid:" + ent.getId() + "/";
		}
		return location;
	}

	private void reconnectExitLinkToLoopDuplicate(Link link, String loopDuplicateLocation) {
		if (link != null) {
			LOGGER.info("Connecting Loop to Task");
			Map<String, String> attributes = getAttributeMap(link);
			attributes.put("occi.core.source", loopDuplicateLocation);
			String kind = link.getKind().getScheme() + link.getKind().getTerm();
			List<String> mixins = getMixinNames(link);
			String title = attributes.get("occi.core.title");
			String location = deriveLocation(link);
			String owner = getOwnerOfThisElement();

			String tar = loopDuplicateLocation;
			String src = link.getSource().getLocation();
			try {
				EntityManager.addLinkToConfiguration(link.getId(), title, kind, mixins, src, tar, attributes, location,
						owner);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private String duplicateLink(Link link, Map<String, String> map) {
		String uuid = ConfigurationManager.createUUID();
		String title = "Nested Loop to Task Connection";
		String kind = link.getKind().getScheme() + link.getKind().getTerm();
		List<String> mixins = getMixinNames(link);
		Map<String, String> attributes = getAttributeMap(link);
		String location = "/" + link.getKind().getTerm() + "/urn:uuid:" + uuid + "/";
		String owner = getOwnerOfThisElement();

		String src = map.get(link.getSource().getLocation());
		String tar = map.get(link.getTarget().getLocation());
		try {
			EntityManager.addLinkToConfiguration(uuid, title, kind, mixins, src, tar, attributes, location, owner);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return location;

	}

	private void linkLoopToLoop(String loopDuplicateLocation, LoopConnector loopConnector) {

		String uuid = ConfigurationManager.createUUID();
		String title = "Master to Nested Loop Connection";
		Nesteddependency nest = ConnectorFactory.eINSTANCE.createNesteddependency();
		String kind = nest.getKind().getScheme() + nest.getKind().getTerm();
		List<String> mixins = getMixinNames(nest);
		Map<String, String> attributes = getAttributeMap(nest);
		String location = "/" + nest.getKind().getTerm() + "/urn:uuid:" + uuid + "/";
		String owner = getOwnerOfThisElement();

		try {
			EntityManager.addLinkToConfiguration(uuid, title, kind, mixins, loopConnector.getLocation(),
					loopDuplicateLocation, attributes, location, owner);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String duplicateResource(Resource res, String replicaGroup) {
		String uuid = ConfigurationManager.createUUID();
		String summary = res.getSummary();
		String kind = res.getKind().getScheme() + res.getKind().getTerm();
		List<String> mixins = getMixinNames(res);
		Map<String, String> attributes = getAttributeMap(res);
		String location = "/" + res.getKind().getTerm() + "/urn:uuid:" + uuid + "/";
		String owner = getOwnerOfThisElement();
		String title;

		if (res instanceof Loop) {
			title = "Par " + attributes.get("occi.core.title");
			mixins.removeIf(str -> (str.equals("http://schemas.ugoe.cs.rwm/workflow#parallelloop")));
			attributes.remove("parallel.replicate.number");
			attributes.put("workflow.decision.input", this.loopSplit.get(0));
			this.loopSplit.remove(0);
		} else {
			title = "Replica " + attributes.get("occi.core.title");
			mixins.add("http://schemas.ugoe.cs.rwm/workflow#replica");
			if (res.getId().startsWith("urn:uuid:")) {
				attributes.put("replica.source.id", res.getId());
			} else {
				attributes.put("replica.source.id", "urn:uuid:" + res.getId());
			}
			attributes.put("replica.group", String.valueOf(replicaGroup));
		}
		attributes.put("occi.core.title", title);

		try {
			EntityManager.addResourceToConfiguration(uuid, title, summary, kind, mixins, attributes, location, owner);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return location;
	}

	private String getOwnerOfThisElement() {
		// Currently no acces is granzed to the actual owner of this element
		// To fix this adjustment have to be made on the Mart server
		// For example the static list of owners could be made puclic
		// To gain information about this elements owner
		// If necessary this attribute could be set via a java property
		return ConfigurationManager.DEFAULT_OWNER;
	}

	private Map<String, String> getAttributeMap(Entity ent) {
		Map<String, String> map = new HashMap<>();
		for (AttributeState attr : ent.getAttributes()) {
			map.put(attr.getName(), attr.getValue());
		}
		for (MixinBase mixB : ent.getParts()) {
			for (AttributeState attr : mixB.getAttributes()) {
				map.put(attr.getName(), attr.getValue());
			}
		}
		return map;
	}

	private List<String> getMixinNames(Entity ent) {
		List<String> list = new ArrayList<>();
		for (MixinBase mixB : ent.getParts()) {
			list.add(mixB.getMixin().getScheme().toLowerCase() + mixB.getMixin().getTerm());
		}
		return list;
	}

	private List<String> calculateLoopSplit() {
		List<String> splits = new ArrayList<String>();
		String splitter = getSplitter();
		List<String> input = Arrays.asList(loop.getWorkflowDecisionInput().split(splitter));
		int partitionSize = (int) Math.ceil(input.size() / (double) numberOfReplicas);
		List<List<String>> partitions = Lists.partition(input, partitionSize);
		for (List<String> list : partitions) {
			if (String.join(splitter, list).isEmpty() == false) {
				splits.add(String.join(splitter, list));
			}
		}
		return splits;
	}

	private String getSplitter() {
		for (MixinBase mixB : loop.getParts()) {
			if (mixB instanceof Foreach) {
				Foreach fe = (Foreach) mixB;
				return fe.getLoopItemDelimiter();
			}
		}
		return ";";
	}
}
