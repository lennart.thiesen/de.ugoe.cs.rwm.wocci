/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Tue Aug 28 15:09:33 CEST 2018 from platform:/resource/workflow/model/workflow.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.wocci.connector;

import de.ugoe.cs.rwm.wocci.connector.decision.gatherer.Gatherer;
import de.ugoe.cs.rwm.wocci.connector.decision.gatherer.GathererFactory;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.Processor;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.ProcessorFactory;
import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import workflow.Executionlink;
import workflow.Status;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/workflow# - term: task - title: Task
 */
public class DecisionConnector extends workflow.impl.DecisionImpl {
	/**
	 * Initialize the logger.
	 */
	private static Logger LOGGER = LoggerFactory.getLogger(DecisionConnector.class);
	private Gatherer gatherer;
	private Processor processor;

	// Start of user code Decisionconnector_constructor
	/**
	 * Constructs a Decision connector.
	 */
	DecisionConnector() {
		LOGGER.debug("Constructor called on " + this);
	}
	// End of user code
	//
	// OCCI CRUD callback operations.
	//

	// Start of user code DecisionocciCreate
	/**
	 * Called when this Decision instance is completely created.
	 */
	@Override
	public void occiCreate() {
		LOGGER.debug("occiCreate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Decision_occiRetrieve_method
	/**
	 * Called when this Decision instance must be retrieved.
	 */
	@Override
	public void occiRetrieve() {
		LOGGER.debug("occiRetrieve() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Decision_occiUpdate_method
	/**
	 * Called when this Decision instance is completely updated.
	 */
	@Override
	public void occiUpdate() {
		LOGGER.debug("occiUpdate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code DecisionocciDelete_method
	/**
	 * Called when this Decision instance will be deleted.
	 */
	@Override
	public void occiDelete() {
		LOGGER.debug("occiDelete() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	//
	// Decision actions.
	//

	// Start of user code Decision_Kind_schedule_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: schedule - title:
	 * schedule
	 */
	@Override
	public void schedule() {
		LOGGER.debug("Action schedule() called on " + this);
		processor = ProcessorFactory.getProcessor(this);
		// Decision State Machine.
		switch (getWorkflowTaskState().getValue()) {
		case Status.ERROR_VALUE:
			LOGGER.debug("Fire transition(state=error, action=\"schedule\")...");
			processor.scheduleDecision();
			break;

		case Status.FINISHED_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"schedule\")...");
			processor.scheduleDecision();
			break;

		case Status.SKIPPED_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"schedule\")...");
			processor.scheduleDecision();
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Decision_Kind_stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: stop - title:
	 */
	@Override
	public void stop() {
		LOGGER.debug("Action stop() called on " + this);

		// Decision State Machine.
		switch (getWorkflowTaskState().getValue()) {

		case Status.ACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=active, action=\"stop\")...");
			// TODO Implement transition(state=active, action="stop")
			setWorkflowTaskState(Status.INACTIVE);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Task_Kind_stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: skip - title:
	 */
	@Override
	public void skip() {
		LOGGER.debug("Action stop() called on " + this);

		// Task State Machine.
		switch (getWorkflowTaskState().getValue()) {

		case Status.SCHEDULED_VALUE:
			LOGGER.debug("Fire transition(state=scheduled, action=\"skip\")...");
			// TODO Implement transition(state=active, action="stop")
			setWorkflowTaskState(Status.SKIPPED);
			break;
		case Status.INACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=scheduled, action=\"skip\")...");
			// TODO Implement transition(state=active, action="stop")
			setWorkflowTaskState(Status.SKIPPED);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Decision_Kind_start_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: start - title: start
	 */
	@Override
	public void start() {
		LOGGER.debug("Action start() called on " + this);
		gatherer = GathererFactory.getGatherer(this);
		processor = ProcessorFactory.getProcessor(this);
		LOGGER.info("Parts: " + this.getParts());
		LOGGER.info("Chosen gatherer: " + gatherer.getClass());
		LOGGER.info("Chosen processor: " + processor.getClass());

		// Decision State Machine.
		switch (getWorkflowTaskState().getValue()) {

		case Status.SCHEDULED_VALUE:
			LOGGER.debug("Fire transition(state=scheduled, action=\"start\")...");
			if (ModelUtility.getSpecificLinks(this, Executionlink.class).isEmpty() == false) {
				setWorkflowTaskState(Status.ACTIVE);
				gatherer.gatherRuntimeInformation();
				processor.processGatheredInformation();
				setWorkflowTaskState(Status.FINISHED);
			} else{
				if(getWorkflowDecisionInput() != null && getWorkflowDecisionInput().equals("") == false) {
					setWorkflowTaskState(Status.ACTIVE);
					processor.processGatheredInformation();
					setWorkflowTaskState(Status.FINISHED);
				}
			}
			break;
		case Status.INACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"start\")...");
			if (ModelUtility.getSpecificLinks(this, Executionlink.class).isEmpty() == false) {
				setWorkflowTaskState(Status.ACTIVE);
				gatherer.gatherRuntimeInformation();
				processor.processGatheredInformation();
				setWorkflowTaskState(Status.FINISHED);
			} else{
				if(getWorkflowDecisionInput() != null && getWorkflowDecisionInput().equals("") == false) {
					setWorkflowTaskState(Status.ACTIVE);
					processor.processGatheredInformation();
					setWorkflowTaskState(Status.FINISHED);
				}
			}
			break;
		default:
			break;
		}
	}
	// End of user code

	public Gatherer getGatherer() {
		return gatherer;
	}

	public void setGatherer(Gatherer gatherer) {
		this.gatherer = gatherer;
	}

	public Processor getProcessor() {
		return processor;
	}

	public void setProcessor(Processor processor) {
		this.processor = processor;
	}
}
