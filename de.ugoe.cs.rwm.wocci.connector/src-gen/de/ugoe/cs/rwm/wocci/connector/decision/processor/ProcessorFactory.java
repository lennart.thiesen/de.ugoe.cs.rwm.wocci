package de.ugoe.cs.rwm.wocci.connector.decision.processor;

import de.ugoe.cs.rwm.wocci.connector.DecisionConnector;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.loop.ForEachLoopProcessor;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.loop.ForLoopProcessor;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.loop.WhileLoopProcessor;
import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import org.eclipse.cmf.occi.core.MixinBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import workflow.*;

public abstract class ProcessorFactory {
	private final static Logger LOGGER = LoggerFactory.getLogger(DecisionConnector.class);

	public static Processor getProcessor(Decision decision) {
		Processor processor = null;
		if (decision instanceof Loop) {
			MixinBase mix = ModelUtility.getLoopMixin(decision);

			if (mix instanceof For) {
				processor = new ForLoopProcessor(decision);
			} else if (mix instanceof Foreach) {
				processor = new ForEachLoopProcessor(decision);
			} else if (mix instanceof While) {
				processor = new WhileLoopProcessor(decision);
			} else {
				processor = new DecisionProcessor(decision);
			}
		} else {
			processor = new DecisionProcessor(decision);
		}

		return processor;
	}
}
