package de.ugoe.cs.rwm.wocci.connector.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.core.OCCIFactory;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.modmacao.occi.platform.Component;

import workflow.*;

public class ModelUtility {
	public static <T> List<T> getSpecificLinks(Task task, Class<T> clazz) {
		List<T> sLinks = new ArrayList<T>();
		for (Link link : task.getLinks()) {
			if (clazz.isInstance(link)) {
				sLinks.add(clazz.cast(link));
			}

		}
		return sLinks;
	}

	public static AttributeState getTaskStateMessage(Task task) {
		for (AttributeState as : task.getAttributes()) {
			if (as.getName().equals("workflow.task.state.message")) {
				return as;
			}
		}
		AttributeState as = OCCIFactory.eINSTANCE.createAttributeState();
		as.setName("workflow.task.state.message");
		task.getAttributes().add(as);
		return as;
	}

	public static MixinBase getLoopMixin(Decision decision) {
		for (MixinBase part : decision.getParts()) {
			if (part != null && part instanceof While || part instanceof Foreach || part instanceof For) {
				return part;
			}
		}
		return null;
	}

	public static Loopiteration getLoopIterationMixin(Task task) {
		for (MixinBase part : task.getParts()) {
			if (part != null && part instanceof Loopiteration) {
				return (Loopiteration) part;
			}
		}
		return null;
	}

	public static void createLoopIterationMixin(Task task) {
		MixinBase iteration = WorkflowFactory.eINSTANCE.createLoopiteration();
		task.getParts().add(iteration);
	}

	public static void updateLoopIterationInformation(Task task, String itVarName, String itVarValue) {
		Loopiteration iteration = getLoopIterationMixin(task);
		iteration.setLoopIterationVarValue(itVarValue);
		iteration.setLoopIterationVarName(itVarName);
		updateTaskExecutableWithLoopInformation(task, iteration);
	}

	public static void updateTaskExecutableWithLoopInformation(Task task, Loopiteration iteration) {
		for (Executionlink execL : ModelUtility.getSpecificLinks(task, Executionlink.class)) {
			if (execL.getTarget() instanceof Component) {
				Component executable = (Component) execL.getTarget();
				for (AttributeState attr : executable.getAttributes()) {
					if (attr.getName().equals(iteration.getLoopIterationVarName())) {
						attr.setValue(iteration.getLoopIterationVarValue());
					}
				}
				for (MixinBase mixB : executable.getParts()) {
					for (AttributeState attr : mixB.getAttributes()) {
						if (attr.getName().equals(iteration.getLoopIterationVarName())) {
							attr.setValue(iteration.getLoopIterationVarValue());
						}
					}
				}
			}
		}
	}

	public static Loopiteration createLoopiteration(String itVarName, String itVarValue) {
		Loopiteration iter = WorkflowFactory.eINSTANCE.createLoopiteration();
		iter.setLoopIterationVarName(itVarName);
		iter.setLoopIterationVarValue(itVarValue);
		return iter;
	}

	public static EList<Taskdependency> getTaskDependencyLinks(EList<Link> links) {
		EList<Taskdependency> dependencyLinks = new BasicEList<Taskdependency>();
		for (Link link : links) {
			if (link instanceof Taskdependency
					|| (link.getKind().getScheme().equals("http://schemas.ugoe.cs.rwm/workflow#")
							&& (link.getKind().getTerm().equals("controlflowlink")
									|| link.getKind().getTerm().equals("datalink")))) {
				dependencyLinks.add((Taskdependency) link);
			}
		}
		return dependencyLinks;
	}
}
