/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Tue Aug 28 15:09:33 CEST 2018 from platform:/resource/workflow/model/workflow.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.wocci.connector;

/**
 * Connector EFactory for the OCCI extension: - name: workflow - scheme:
 * http://schemas.ugoe.cs.rwm/workflow#
 */
public class ConnectorFactory extends workflow.impl.WorkflowFactoryImpl {

	/*
	public ConnectorFactory (){
		PostConnectorConstructor pc = new PostConnectorConstructor(10);
		Thread thread = new Thread(pc);
		thread.start();

	}*/
	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: task - title: Task
	 */
	@Override
	public workflow.Task createTask() {
		return new TaskConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: decision - title: Decision
	 */
	@Override
	public workflow.Decision createDecision() {
		return new DecisionConnector();
	}

	/*
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: loop - title: Loop
	 */
	@Override
	public workflow.Loop createLoop() {
		return new LoopConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: nesteddependency - title:
	 */
	@Override
	public workflow.Nesteddependency createNesteddependency() {
		return new NesteddependencyConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: executionlink - title: ExecutionLink
	 */
	@Override
	public workflow.Executionlink createExecutionlink() {
		return new ExecutionlinkConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: datalink - title: DataLink
	 */
	@Override
	public workflow.Datalink createDatalink() {
		return new DatalinkConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: controlflowlink - title: ControlflowLink
	 */
	@Override
	public workflow.Controlflowlink createControlflowlink() {
		return new ControlflowlinkConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: taskdependency - title: TaskDependency
	 */
	@Override
	public workflow.Taskdependency createTaskdependency() {
		return new TaskdependencyConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: platformdependency - title: PlatformDependency
	 */
	@Override
	public workflow.Platformdependency createPlatformdependency() {
		return new PlatformdependencyConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: storagecanal - title: StorageCanal
	 */
	@Override
	public workflow.Storagecanal createStoragecanal() {
		return new StoragecanalConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: networkcanal - title: NetworkCanal
	 */
	@Override
	public workflow.Networkcanal createNetworkcanal() {
		return new NetworkcanalConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: remotedatacanal - title: FileSystem
	 */
	@Override
	public workflow.Remotedatacanal createRemotedatacanal() {
		return new RemotedatacanalConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: profile - title: Profile
	 */
	@Override
	public workflow.Profile createProfile() {
		return new ProfileConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: localcanal - title: LocalCanal
	 */
	@Override
	public workflow.Localcanal createLocalcanal() {
		return new LocalcanalConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: parallelloop - title: parallelloop
	 */
	@Override
	public workflow.Parallelloop createParallelloop() {
		return new ParallelloopConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme: http://schemas.ugoe.cs.rwm/workflow#
	 * - term: replica - title: replica
	 */
	@Override
	public workflow.Replica createReplica() {
		return new ReplicaConnector();
	}

}
