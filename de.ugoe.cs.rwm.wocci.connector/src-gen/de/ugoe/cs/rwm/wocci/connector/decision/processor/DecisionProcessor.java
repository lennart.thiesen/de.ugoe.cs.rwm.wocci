package de.ugoe.cs.rwm.wocci.connector.decision.processor;

import workflow.Controlflowlink;
import workflow.Decision;
import workflow.Status;

import java.util.List;

public class DecisionProcessor extends AbsProcessor {

	public DecisionProcessor(Decision decision) {
		super(decision);
	}

	@Override
	protected void performMixinSpecificTask(List<Controlflowlink> trueCLinks) {

	}

	@Override
	protected void executeSchedulingSpecifics() {
		decision.setWorkflowDecisionInput("");
		rescheduleTaskSequence(decision);
		decision.setWorkflowTaskStateMessage("Decision Rescheduled");
		decision.setWorkflowTaskState(Status.SCHEDULED);
	}

}
