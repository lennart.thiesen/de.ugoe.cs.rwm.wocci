package de.ugoe.cs.rwm.wocci.connector;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.wocci.connector.decision.processor.DecisionProcessor;
import workflow.Controlflowguard;
import workflow.Task;
import workflow.WorkflowFactory;

public class DecisionProcessorTest {
	static ConnectorFactory fac = new ConnectorFactory();
	static WorkflowFactory wFac = WorkflowFactory.eINSTANCE;
	static LoopConnector dec;
	static Task task;
	static ControlflowlinkConnector cLink;
	static Controlflowguard cGuard;
	static DecisionProcessor proc;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void createDecision() {
		dec = (LoopConnector) fac.createLoop();
		proc = new DecisionProcessor(dec);
		task = wFac.createTask();
		task.setTitle("Following Task");

		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();
	}

	@Test
	public void mathExpressionTest() {
		dec.setWorkflowDecisionInput("5");
		dec.setWorkflowDecisionExpression("workflow.decision.input > 10");
		proc.processGatheredInformation();

		// dec.start();
		System.out.println(dec.getWorkflowDecisionResult());
		if (dec.getWorkflowDecisionResult().equals("false")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	@Test
	public void stringExpressionTest() {
		dec.setWorkflowDecisionInput("hallo");
		dec.setWorkflowDecisionExpression("workflow.decision.input.equals('hallo')");
		proc.processGatheredInformation();

		// dec.start();
		System.out.println(dec.getWorkflowDecisionResult());
		if (dec.getWorkflowDecisionResult().equals("true")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	@Test
	public void booleanExpressionTest() {
		dec.setWorkflowDecisionInput("true");
		dec.setWorkflowDecisionExpression("workflow.decision.input == true");
		proc.processGatheredInformation();

		// dec.start();
		System.out.println(dec.getWorkflowDecisionResult());
		if (dec.getWorkflowDecisionResult().equals("true")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	@Test
	public void emptyTest() {
		dec.setWorkflowDecisionInput("");
		assertTrue(dec.getWorkflowDecisionInput().isEmpty());
	}

	@Test
	public void trueCLinkTest() {
		cGuard.setControlflowGuard("false");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);

		dec.setWorkflowDecisionInput("test,test1,test2,test3,test4");
		dec.setWorkflowDecisionExpression("workflow.decision.input.isEmpty()");
		assertTrue(proc.evaluateControlFlowGuards().get(0).equals(cLink));
	}
}
