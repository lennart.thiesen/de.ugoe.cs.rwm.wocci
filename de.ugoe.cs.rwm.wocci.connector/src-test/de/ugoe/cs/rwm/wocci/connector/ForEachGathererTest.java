package de.ugoe.cs.rwm.wocci.connector;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modmacao.occi.platform.Status;

import de.ugoe.cs.rwm.wocci.connector.decision.gatherer.Gatherer;
import de.ugoe.cs.rwm.wocci.connector.decision.gatherer.GathererFactory;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.loop.ForEachLoopProcessor;
import monitoring.Monitorableproperty;
import monitoring.MonitoringFactory;
import monitoring.Sensor;
import workflow.Controlflowguard;
import workflow.Executionlink;
import workflow.Foreach;
import workflow.Task;
import workflow.WorkflowFactory;

public class ForEachGathererTest {
	static ConnectorFactory fac = new ConnectorFactory();
	static WorkflowFactory wFac = WorkflowFactory.eINSTANCE;
	static LoopConnector dec;
	static Task task;
	static ControlflowlinkConnector cLink;
	static Controlflowguard cGuard;
	static ForEachLoopProcessor proc;
	static Foreach fe;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void createDecision() {
		dec = (LoopConnector) fac.createLoop();
		dec.setWorkflowDecisionExpression("workflow.decision.input.isEmpty()");
		fe = wFac.createForeach();
		fe.setLoopItemDelimiter(",");
		dec.getParts().add(fe);
		fe.setLoopItemName("loop.iteration.name");
	}

	@Test
	public void gatheringTest() {
		dec.setWorkflowDecisionInput("test");
		dec.setLoopIterationCount(3);

		createSensor();

		task = wFac.createTask();
		task.setTitle("Following Task");
		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();
		cGuard.setControlflowGuard("false");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);

		Gatherer gatherer = GathererFactory.getGatherer(dec);

		System.out.println("Before: " + dec.getWorkflowDecisionInput());
		gatherer.gatherRuntimeInformation();
		assertEquals(dec.getWorkflowDecisionInput(), "test");

	}

	@Test
	public void gatheringEmptyTest() {
		dec.setWorkflowDecisionInput("");
		dec.setLoopIterationCount(3);

		createSensor();

		task = wFac.createTask();
		task.setTitle("Following Task");
		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();
		cGuard.setControlflowGuard("false");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);

		Gatherer gatherer = GathererFactory.getGatherer(dec);

		System.out.println("Before: " + dec.getWorkflowDecisionInput());
		gatherer.gatherRuntimeInformation();
		assertEquals(dec.getWorkflowDecisionInput(), "");

	}

	private void createSensor() {
		Sensor sens = MonitoringFactory.eINSTANCE.createSensor();
		Monitorableproperty monprop = MonitoringFactory.eINSTANCE.createMonitorableproperty();
		monprop.setTarget(task);
		monprop.setSource(sens);
		monprop.setMonitoringProperty("monproperty");
		monprop.setMonitoringResult("test,test1,test2,test3,test4");
		sens.getLinks().add(monprop);
		sens.setOcciAppState(Status.ACTIVE);

		Executionlink pdep = WorkflowFactory.eINSTANCE.createExecutionlink();
		pdep.setSource(dec);
		pdep.setTarget(sens);
		dec.getLinks().add(pdep);
	}

}
