package de.ugoe.cs.rwm.wocci.connector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.core.OCCIFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modmacao.occi.platform.Component;
import org.modmacao.occi.platform.PlatformFactory;

import de.ugoe.cs.rwm.wocci.connector.decision.processor.loop.ForEachLoopProcessor;
import workflow.Controlflowguard;
import workflow.Controlflowlink;
import workflow.Executionlink;
import workflow.Foreach;
import workflow.Loopiteration;
import workflow.Task;
import workflow.WorkflowFactory;

public class ForEachProcessorTest {
	static ConnectorFactory fac = new ConnectorFactory();
	static WorkflowFactory wFac = WorkflowFactory.eINSTANCE;
	static LoopConnector dec;
	static TaskConnector task;
	static ControlflowlinkConnector cLink;
	static Controlflowguard cGuard;
	static ForEachLoopProcessor proc;
	static Foreach fe;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void createDecision() {
		dec = (LoopConnector) fac.createLoop();
		fe = wFac.createForeach();
		fe.setLoopItemDelimiter(",");
		dec.getParts().add(fe);
		proc = new ForEachLoopProcessor(dec);
		// task = wFac.createTask();
		task = (TaskConnector) fac.createTask();
		task.setTitle("Following Task");
		fe.setLoopItemName("loop.iteration.name");
		dec.setWorkflowDecisionExpression("workflow.decision.input.isEmpty()");
	}

	@Test
	public void singleAnnotationTest() {
		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();

		cGuard.setControlflowGuard("false");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);

		dec.setWorkflowDecisionInput("test,test1,test2,test3,test4");
		assertTrue(task.getParts().isEmpty());
		proc.processGatheredInformation();
		assertTrue(dec.getLinks().get(0).getTarget().equals(task));
		assertTrue(dec.getWorkflowDecisionResult().equals("false"));

		assertTrue(task.getParts().get(0) instanceof Loopiteration);

		Loopiteration iter = (Loopiteration) task.getParts().get(0);
		assertEquals(iter.getLoopIterationVarValue(), "test");
		assertEquals(iter.getLoopIterationVarName(), "loop.iteration.name");
		assertEquals(dec.getWorkflowDecisionInput(), "test1,test2,test3,test4");
	}

	@Test
	public void componentTest() {
		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();

		cGuard.setControlflowGuard("false");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);
		Component comp = PlatformFactory.eINSTANCE.createComponent();
		AttributeState attr = OCCIFactory.eINSTANCE.createAttributeState();
		attr.setName("loop.iteration.name");
		attr.setValue("toAdjustAtRuntime");
		comp.getAttributes().add(attr);

		Executionlink execL = WorkflowFactory.eINSTANCE.createExecutionlink();
		execL.setTarget(comp);
		execL.setSource(task);
		task.getLinks().add(execL);

		dec.setWorkflowDecisionInput("test,test1,test2,test3,test4");
		assertTrue(task.getParts().isEmpty());
		proc.processGatheredInformation();
		assertTrue(dec.getLinks().get(0).getTarget().equals(task));
		assertTrue(dec.getWorkflowDecisionResult().equals("false"));

		assertTrue(task.getParts().get(0) instanceof Loopiteration);

		Loopiteration iter = (Loopiteration) task.getParts().get(0);
		assertEquals(iter.getLoopIterationVarValue(), "test");
		assertEquals(iter.getLoopIterationVarName(), "loop.iteration.name");
		assertEquals(dec.getWorkflowDecisionInput(), "test1,test2,test3,test4");
		System.out.println(comp);
		System.out.println(comp.getAttributes());
		assertEquals(attr.getValue(), iter.getLoopIterationVarValue());
	}

	@Test
	public void componentAttributeInMixinTest() {
		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();

		cGuard.setControlflowGuard("false");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);
		Component comp = PlatformFactory.eINSTANCE.createComponent();
		AttributeState attr = OCCIFactory.eINSTANCE.createAttributeState();
		MixinBase mixB = PlatformFactory.eINSTANCE.createDatabase();
		mixB.getAttributes().add(attr);
		attr.setName("loop.iteration.name");
		attr.setValue("toAdjustAtRuntime");
		comp.getParts().add(mixB);
		Executionlink execL = WorkflowFactory.eINSTANCE.createExecutionlink();
		execL.setTarget(comp);
		execL.setSource(task);
		task.getLinks().add(execL);

		dec.setWorkflowDecisionInput("test,test1,test2,test3,test4");
		assertTrue(task.getParts().isEmpty());
		proc.processGatheredInformation();
		assertTrue(dec.getLinks().get(0).getTarget().equals(task));
		assertTrue(dec.getWorkflowDecisionResult().equals("false"));

		assertTrue(task.getParts().get(0) instanceof Loopiteration);

		Loopiteration iter = (Loopiteration) task.getParts().get(0);
		assertEquals(iter.getLoopIterationVarValue(), "test");
		assertEquals(iter.getLoopIterationVarName(), "loop.iteration.name");
		assertEquals(dec.getWorkflowDecisionInput(), "test1,test2,test3,test4");
		System.out.println(comp);
		System.out.println(comp.getParts().get(0).getAttributes());
		assertEquals(attr.getValue(), iter.getLoopIterationVarValue());
	}

	@Test
	public void multipleAnnotationTest() {
		Task task2 = fac.createTask();
		Controlflowlink cLink2 = wFac.createControlflowlink();
		task.getLinks().add(cLink2);
		cLink2.setTarget(task2);

		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();

		cGuard.setControlflowGuard("false");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);

		dec.setWorkflowDecisionInput("test,test1,test2,test3,test4");
		assertTrue(task.getParts().isEmpty());
		proc.processGatheredInformation();
		assertTrue(dec.getLinks().get(0).getTarget().equals(task));
		assertTrue(dec.getWorkflowDecisionResult().equals("false"));

		assertTrue(task.getParts().get(0) instanceof Loopiteration);

		Loopiteration iter = (Loopiteration) task.getParts().get(0);
		assertEquals(iter.getLoopIterationVarValue(), "test");
		assertEquals(dec.getWorkflowDecisionInput(), "test1,test2,test3,test4");

		assertTrue(task2.getParts().get(0) instanceof Loopiteration);
		iter = (Loopiteration) task2.getParts().get(0);
		assertEquals(iter.getLoopIterationVarValue(), "test");
		assertEquals(dec.getWorkflowDecisionInput(), "test1,test2,test3,test4");
	}

	@Test
	public void secondIterationAnnotationTest() {
		Task task2 = fac.createTask();
		Controlflowlink cLink2 = wFac.createControlflowlink();
		task.getLinks().add(cLink2);
		cLink2.setTarget(task2);

		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();

		cGuard.setControlflowGuard("false");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);

		dec.setWorkflowDecisionInput("test,test1,test2,test3,test4");
		assertTrue(task.getParts().isEmpty());
		proc.processGatheredInformation();
		assertTrue(dec.getLinks().get(0).getTarget().equals(task));
		assertTrue(dec.getWorkflowDecisionResult().equals("false"));

		assertTrue(task.getParts().get(0) instanceof Loopiteration);

		Loopiteration iter = (Loopiteration) task.getParts().get(0);
		assertEquals(iter.getLoopIterationVarValue(), "test");
		assertEquals(dec.getWorkflowDecisionInput(), "test1,test2,test3,test4");

		assertTrue(task2.getParts().get(0) instanceof Loopiteration);
		iter = (Loopiteration) task2.getParts().get(0);
		assertEquals(iter.getLoopIterationVarValue(), "test");
		assertEquals(dec.getWorkflowDecisionInput(), "test1,test2,test3,test4");

		proc.processGatheredInformation();
		assertEquals(iter.getLoopIterationVarValue(), "test1");
		assertEquals(dec.getWorkflowDecisionInput(), "test2,test3,test4");

	}

	@Test
	public void finalIterationTest() {
		dec.setWorkflowDecisionInput("test1");
		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();

		cGuard.setControlflowGuard("false");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);

		assertTrue(task.getParts().isEmpty());
		proc.processGatheredInformation();
		assertTrue(dec.getLinks().get(0).getTarget().equals(task));
		assertTrue(dec.getWorkflowDecisionResult().equals("false"));

		assertTrue(task.getParts().get(0) instanceof Loopiteration);

		Loopiteration iter = (Loopiteration) task.getParts().get(0);
		assertEquals(iter.getLoopIterationVarValue(), "test1");
		assertEquals(dec.getWorkflowDecisionInput(), "");

	}

	@Test
	public void processEmptyInputTest() {
		dec.setWorkflowDecisionInput("");
		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();

		cGuard.setControlflowGuard("true");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);

		proc.processGatheredInformation();
	}

	@Test
	public void forEachEmptyExpressionTest() {
		String emptyString = "";
		dec.setWorkflowDecisionInput(emptyString);
		proc.processGatheredInformation();

		if (dec.getWorkflowDecisionResult().equals("true")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	@Test
	public void forEachFullExpressionTest() {
		dec.setWorkflowDecisionInput("test,test1,test2,test3,test4");
		proc.processGatheredInformation();
		if (dec.getWorkflowDecisionResult().equals("false")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	@Test
	public void forEachDelimiterTest() {
		cLink = (ControlflowlinkConnector) fac.createControlflowlink();
		cGuard = wFac.createControlflowguard();

		cGuard.setControlflowGuard("false");
		cLink.getParts().add(cGuard);
		cLink.setTarget(task);
		dec.getLinks().add(cLink);
		fe.setLoopItemDelimiter(";");

		dec.setWorkflowDecisionInput("test;test1;test2;test3;test4");
		assertTrue(task.getParts().isEmpty());
		proc.processGatheredInformation();
		assertTrue(dec.getLinks().get(0).getTarget().equals(task));
		assertTrue(dec.getWorkflowDecisionResult().equals("false"));

		assertTrue(task.getParts().get(0) instanceof Loopiteration);

		Loopiteration iter = (Loopiteration) task.getParts().get(0);
		assertEquals("test", iter.getLoopIterationVarValue());
		assertEquals("test1;test2;test3;test4", dec.getWorkflowDecisionInput());
	}

}
