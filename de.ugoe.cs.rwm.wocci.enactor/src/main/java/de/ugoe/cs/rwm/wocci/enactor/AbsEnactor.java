package de.ugoe.cs.rwm.wocci.enactor;

import java.util.Observable;

import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.utility.ModelUtility;
import de.ugoe.cs.rwm.wocci.utility.WorkflowUtility;
import workflow.Decision;
import workflow.Loop;
import workflow.Replica;
import workflow.Task;

public abstract class AbsEnactor extends Observable implements WorkflowEnactor {
	protected Connector conn;
	protected String jobHistoryPath;
	protected org.eclipse.emf.ecore.resource.Resource designtimeModel;

	public abstract void enactWorkflow(org.eclipse.emf.ecore.resource.Resource runtimeModel);

	protected EList<Resource> getTasksForExecution(org.eclipse.emf.ecore.resource.Resource runtimeModel) {
		EList<Resource> tasksToBeExecuted = new BasicEList<Resource>();
		for (Resource task : ModelUtility.getTasksAsResource(runtimeModel)) {
			if (isReadyForExecution(task)) {
				LOG.debug("Task ready for execution: " + task.getTitle());
				tasksToBeExecuted.add(task);
			}
		}
		return tasksToBeExecuted;
	}

	protected boolean PlatDependencyCreated(Resource task) {
		if (designtimeModel == null || task instanceof Loop) {
			return true;
		}

		String origTaskId = task.getId();
		for (MixinBase mixB : task.getParts()) {
			if (mixB instanceof Replica) {
				origTaskId = ((Replica) mixB).getReplicaSourceId();
			}
		}

		for (Resource res : ModelUtility.getTasksAsResource(designtimeModel)) {
			if (res.getId().equals(origTaskId)) {
				if (ModelUtility.getPlatformDependencyLinks(res.getLinks()).size() == ModelUtility
						.getPlatformDependencyLinks(task.getLinks()).size()) {
					return true;
				}
			}
		}

		return false;
	}

	protected boolean isReadyForExecution(Resource task) {
		// boolean deployed = isDeployed(comp);
		boolean taskDependenciesFulfilled = WorkflowUtility.isTaskDependencyFulfilled(task);
		boolean platformDependenciesFulfilled = WorkflowUtility.isPlatformDependencyFulfilled(task);
		boolean scheduled = WorkflowUtility.isScheduled((Task) task);
		// This check is needed due to container instances requiring time to spawn
		boolean executableHostRunning = WorkflowUtility.isExecutableHostRunning(task);
		boolean hasConnectionToPlatform = WorkflowUtility.hasConnectionToPlatform(task);
		boolean platDependencyCreated = PlatDependencyCreated(task);

		if (task instanceof Decision) {
			Decision dec = (Decision) task;
			boolean hasDecisionInput = WorkflowUtility.hasDecisionInput(dec);
			LOG.debug(
					"Decision: " + task.getTitle() + " | State: " + ((Decision) task).getWorkflowTaskState().toString()
							+ " | TaskDependency: " + taskDependenciesFulfilled + " | PlatDep Created: "
							+ platDependencyCreated + " | PlatformDependency: " + platformDependenciesFulfilled
							+ " |Decision Input: " + hasDecisionInput);
			if (scheduled && platformDependenciesFulfilled && taskDependenciesFulfilled && executableInRuntimeModel(dec)
					&& executableHostRunning && (hasDecisionInput || hasConnectionToPlatform)) {
				// Workaround:
				// This checks results in less requests send when triggering a decision,
				// as some time is needed until the decision reaches the active state.
				// It should be noted that this beaviour only occurs within the control loop of
				// the parallel performer.
				// Improvement: Store list of requests send by the enactor

				if (dec.getWorkflowTaskStateMessage() != null
						&& dec.getWorkflowTaskStateMessage().equals("Waiting for Decision Input")) {
					return false;
				}
				return true;
			}
		} else {
			LOG.debug("Task: " + task.getTitle() + " | TaskDependency: " + taskDependenciesFulfilled
					+ " | PlatformDependency: " + platformDependenciesFulfilled + " | Connection to Platform: "
					+ hasConnectionToPlatform + " | Executable Host Running: " + executableHostRunning);
			if (scheduled && platformDependenciesFulfilled && executableHostRunning && taskDependenciesFulfilled
					&& hasConnectionToPlatform) {
				return true;
			}
		}
		return false;
	}

	private boolean executableInRuntimeModel(Decision runDecision) {
		Resource res = getResourceFromDesign(runDecision.getId());
		if (res == null) {
			return true;
		}
		if (ModelUtility.getExecutionLinks(res.getLinks()).isEmpty()) {
			return true;
		} else {
			return ModelUtility.getExecutionLinks(runDecision.getLinks()).size() == ModelUtility
					.getExecutionLinks(res.getLinks()).size();
		}
	}

	private Resource getResourceFromDesign(String id) {
		for (Resource res : de.ugoe.cs.rwm.docci.ModelUtility.getOCCIConfiguration(designtimeModel).getResources()) {
			if (res.getId().equals(id)) {
				return res;
			}
		}
		return null;
	}

	protected void logTaskExecution(EList<org.eclipse.cmf.occi.core.Resource> tasks) {
		StringBuilder str = new StringBuilder();
		str.append("Executing Tasks: ");
		for (org.eclipse.cmf.occi.core.Resource res : tasks) {
			str.append(res.getTitle() + " | ");
		}
		LOG.info(str.toString());
	}

	public Connector getConnector() {
		return this.conn;
	}

	@Override
	public void setJobHistoryPath(String jobHistoryPath) {
		this.jobHistoryPath = jobHistoryPath;
	}

	@Override
	public String getJobHistoryPath() {
		return jobHistoryPath;
	}

	public void setDesigntimeModel(org.eclipse.emf.ecore.resource.Resource dm) {
		this.designtimeModel = dm;
	}
}
