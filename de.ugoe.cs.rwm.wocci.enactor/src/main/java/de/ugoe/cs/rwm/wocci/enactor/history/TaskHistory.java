package de.ugoe.cs.rwm.wocci.enactor.history;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.ugoe.cs.rwm.wocci.utility.WorkflowUtility;
import workflow.Loop;
import workflow.Task;

public class TaskHistory {
	private String taskName;
	private long startDate;
	private Date readableStartDate;
	private long endDate;
	private Date readableEndDate;
	private long duration;

	private String taskId;
	private static HashMap<String, TaskHistory> loopMapping = new HashMap<>();
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<TaskHistory> taskHistory = new ArrayList<>();

	public TaskHistory(Task task, long startDate, long endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.readableStartDate = new Date(startDate);
		this.readableEndDate = new Date(endDate);

		long durationInMilliSeconds = Math.abs(endDate - startDate);
		this.duration = TimeUnit.MILLISECONDS.convert(durationInMilliSeconds, TimeUnit.MILLISECONDS);
		this.taskName = task.getTitle();
		this.taskId = task.getId();
		if (task instanceof Loop) {
			Loop loop = (Loop) task;
			// loopMapping.put(loop.getId(), this);
			for (Task loopedTask : WorkflowUtility.getLoopedTasks(loop)) {
				this.taskHistory = new ArrayList<>();
				loopMapping.put(loopedTask.getId(), this);
			}
		}

		if (loopMapping.get(taskId) != null) {
			loopMapping.get(taskId).taskHistory.add(this);
		}

	}

	public TaskHistory() {

	}

	public void store(String jobHistoryPath) {
		if (jobHistoryPath != null && jobHistoryPath.equals("") == false) {
			if (loopMapping.get(taskId) != null) {
				TaskHistory loopHistory = loopMapping.get(taskId);
				loopHistory.endDate = this.endDate;
				long durationInMilliSeconds = Math.abs(loopHistory.endDate - loopHistory.startDate);
				loopHistory.duration = TimeUnit.MILLISECONDS.convert(durationInMilliSeconds, TimeUnit.MILLISECONDS);
				loopHistory.store(jobHistoryPath);
			} else {
				ObjectMapper objectMapper = new ObjectMapper();
				try {
					File logDir = new File(jobHistoryPath);
					if (!logDir.exists()) {
						System.out.println("Creating log Directory.");
						logDir.mkdirs();
					}
					objectMapper.writeValue(new File(jobHistoryPath + "/" + taskId + ".json"), this);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} else {
			System.out.println("No job history path set. Can not store task history!");
		}
	}

	public String getTaskName() {
		return taskName;
	}

	public long getStartDate() {
		return startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public long getDuration() {
		return duration;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public List<TaskHistory> getTaskHistory() {
		return taskHistory;
	}

	public Date getReadableStartDate() {
		return readableStartDate;
	}

	public void setReadableStartDate(Date readableStartDate) {
		this.readableStartDate = readableStartDate;
	}

	public Date getReadableEndDate() {
		return readableEndDate;
	}

	public void setReadableEndDate(Date readableEndDate) {
		this.readableEndDate = readableEndDate;
	}
}
