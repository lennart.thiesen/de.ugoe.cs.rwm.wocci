/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.enactor.executor;

import de.ugoe.cs.rwm.wocci.enactor.history.TaskHistory;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.ecore.EObject;

import de.ugoe.cs.rwm.docci.executor.Executor;
import workflow.Platformdependency;
import workflow.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class TaskExecutorSlave extends Observable implements Runnable {
	protected static final Logger LOG = Logger.getLogger(TaskExecutorSlave.class.getName());
	private EObject action;
	private Executor exec;
	private EObject resource;
	private String jobHistoryPath;
	private List<Observer> observers = new ArrayList<>();

	public TaskExecutorSlave(EObject resource, Executor exec, String jobHistoryPath) {
		this.resource = resource;
		this.exec = exec;
		this.action = de.ugoe.cs.rwm.docci.ModelUtility.getAction((Resource) resource, "start");
		this.jobHistoryPath = jobHistoryPath;
	}

	@Override
	public void run() {
		LOG.info("Execute Task: " + resource + " " + action);
		Task task = (Task) resource;
		if(hasPlatformDep(task) == false){
			try {
				LOG.info("Workaround: Task has no Platformdependency." +
						" Sleeping for 1 second to avoid reachability problems.");
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		startTask(task);
	}

	private void startTask(Resource res){
		long start = System.currentTimeMillis();
		exec.executeOperation("POST", res, action);
		TaskHistory tHist = new TaskHistory((Task) res, start, System.currentTimeMillis());
		tHist.store(jobHistoryPath);
	}

	private boolean hasPlatformDep(Task task) {
		for(Link l: task.getLinks()){
			if(l instanceof Platformdependency){
				return true;
			}
		}
		return false;
	}

	public void addObserver(Observer o){
		this.observers.add(o);
	}
}
