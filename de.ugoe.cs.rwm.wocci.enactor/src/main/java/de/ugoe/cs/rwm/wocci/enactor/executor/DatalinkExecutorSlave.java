/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.enactor.executor;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Entity;
import org.eclipse.emf.ecore.EObject;

import de.ugoe.cs.rwm.docci.executor.Executor;

public class DatalinkExecutorSlave implements Runnable {
	protected static final Logger LOG = Logger.getLogger(DatalinkExecutorSlave.class.getName());
	private EObject action;
	private Executor exec;
	private EObject link;

	public DatalinkExecutorSlave(EObject link, Executor exec) {
		this.link = link;
		this.exec = exec;
		this.action = de.ugoe.cs.rwm.docci.ModelUtility.getAction((Entity) link, "start");
	}

	@Override
	public void run() {
		// Datalink dLink = (Datalink) link;
		/*
		 * for (Link link : task.getLinks()) { if (link instanceof Executionlink) {
		 * Component comp = (Component) link.getTarget(); Action deployComp =
		 * ModelUtility.getAction(comp, "deploy"); exec.executeOperation("POST", comp,
		 * deployComp); Action configureComp = ModelUtility.getAction(comp,
		 * "configure"); exec.executeOperation("POST", comp, configureComp); } }
		 */
		LOG.info("Execute: " + link + " " + action);
		exec.executeOperation("POST", link, action);
	}
}
