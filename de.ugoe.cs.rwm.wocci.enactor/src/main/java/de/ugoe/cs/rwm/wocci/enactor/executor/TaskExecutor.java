/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.enactor.executor;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.common.util.EList;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.executor.ExecutorFactory;

public class TaskExecutor implements Runnable {
	protected static final Logger LOG = Logger.getLogger(TaskExecutor.class.getName());
	private Connector conn;
	private EList<Resource> tasks;
	private String jobHistoryPath;

	public TaskExecutor(EList<Resource> tasks, Connector conn, String jobHistoryPath) {
		this.conn = conn;
		this.tasks = tasks;
		this.jobHistoryPath = jobHistoryPath;
	}

	public void executeTasks() {
		List<TaskExecutorSlave> slaves = new ArrayList<TaskExecutorSlave>();
		List<Thread> threads = new ArrayList<Thread>();
		slaves.addAll(createSubtasks(tasks));
		LOG.debug("Slaves created: " + slaves.size());
		for (TaskExecutorSlave slave : slaves) {
			Thread thread = new Thread(slave);

			threads.add(thread);
			thread.start();
		}
		for (Thread t : threads) {
			LOG.debug("Joining Task Execution Threads");
			try {
				t.join();
				LOG.info("Thread: " + t + "joined");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		LOG.debug("Task Execution Threads joined");

		// exec.executeOperation("POST", task, start);
	}

	private List<TaskExecutorSlave> createSubtasks(EList<Resource> tasks) {
		List<TaskExecutorSlave> slaves = new ArrayList<TaskExecutorSlave>();
		for (Resource task : tasks) {
			Executor exec = ExecutorFactory.getExecutor("Mart", conn);
			TaskExecutorSlave slave = new TaskExecutorSlave(task, exec, jobHistoryPath);
			slaves.add(slave);
		}
		return slaves;
	}

	@Override
	public void run() {
		executeTasks();

	}
}
