/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.enactor;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.emf.ecore.resource.Resource;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CompareWaaSTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestExtensionRegistry.extensionRegistrySetup();
	}

	@Test
	public void compareWaaSSimple() {
		System.out.println(OcciRegistry.getInstance().getRegisteredExtensions());
		Logger.getRootLogger().setLevel(Level.FATAL);
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/model-anonymous.occic"));

		Resource waasModel = ModelUtility.loadOCCIintoEMFResource(occiPath);

		Comparator comp = ComparatorFactory.getComparator("Simple", waasModel, waasModel);

		assertTrue(true);
	}

	@Ignore("Test case only works when both models tested by Mixed and Complex are the same.")
	@Test
	public void compareWaaSComplex() {
		System.out.println(OcciRegistry.getInstance().getRegisteredExtensions());
		Logger.getRootLogger().setLevel(Level.FATAL);
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/model-anonymous.occic"));

		Resource waasModel = ModelUtility.loadOCCIintoEMFResource(occiPath);

		Comparator comp = ComparatorFactory.getComparator("Complex", waasModel, waasModel);

		assertTrue(true);
	}

	@Test
	public void compareWaaSMixed() {
		System.out.println(OcciRegistry.getInstance().getRegisteredExtensions());
		Logger.getRootLogger().setLevel(Level.FATAL);
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/WaaSWithApp.occic"));

		Resource waasModel = ModelUtility.loadOCCIintoEMFResource(occiPath);

		Comparator comp = ComparatorFactory.getComparator("Mixed", waasModel, waasModel);

		assertTrue(true);
	}
}
