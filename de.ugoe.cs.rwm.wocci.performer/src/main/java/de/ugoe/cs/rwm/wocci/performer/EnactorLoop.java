package de.ugoe.cs.rwm.wocci.performer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.emf.ecore.resource.Resource;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.utility.WorkflowUtility;

public class EnactorLoop extends Observable implements Runnable {
	private int cycle;
	private boolean flag = true;
	private WorkflowEnactor enactor;
	private Connector conn;
	private static Path runtimePath = Paths.get(System.getProperty("user.home") + "/.rwm/enactorRuntime.occic");
	List<Observer> observerList = new ArrayList<Observer>();

	public EnactorLoop(WorkflowEnactor enactor, int cycle) {
		this.cycle = cycle;
		this.enactor = enactor;
		this.conn = enactor.getConnector();
	}

	public EnactorLoop(WorkflowEnactor enactor) {
		this.cycle = 500;
		this.enactor = enactor;
		this.conn = enactor.getConnector();
	}

	@Override
	public void run() {
		Resource runtimeModel = PerformerUtility.updatedRuntimeModel(conn, runtimePath);
		try {
			do {
				System.out.println("Hallo Enactor");
				enactor.enactWorkflow(runtimeModel);
				try {
					Thread.sleep(cycle);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runtimeModel = PerformerUtility.updatedRuntimeModel(conn, runtimePath);
			} while (WorkflowUtility.tasksFinished(runtimeModel) == false
					&& WorkflowUtility.containsErrors(runtimeModel) == false && flag == true);
		} catch (Exception e) {
			for (Observer obs : observerList) {
				obs.update(this, e);
			}
		}
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public void addObserver(Observer obs) {
		this.observerList.add(obs);
	}
}
