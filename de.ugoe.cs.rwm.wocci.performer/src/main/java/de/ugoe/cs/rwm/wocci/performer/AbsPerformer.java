package de.ugoe.cs.rwm.wocci.performer;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;

import com.google.common.io.Files;

import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.DynamicEnactor;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.performer.history.WocciHistory;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureSchedulingException;
import de.ugoe.cs.rwm.wocci.scheduler.BatchScheduler;
import de.ugoe.cs.rwm.wocci.utility.WorkflowUtility;

public abstract class AbsPerformer implements Performer {
	static final Logger LOG = Logger.getLogger(Performer.class.getName());
	protected Connector conn;
	protected WorkflowEnactor enactor;
	protected ArchitectureScheduler scheduler;
	protected Boolean flag = true;
	protected static Path reqRuntimePath = Paths.get(System.getProperty("user.home") + "/.rwm/reqRuntime.occic");
	private static Path runtimePath = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
	private static final String JOBHISTORYPATH = System.getProperty("user.home") + "/.rwm/job_history/";

	abstract void startWorkflow(Resource designTimeModel);

	protected AbsPerformer(Connector conn) {
		this.conn = conn;
		this.enactor = new DynamicEnactor(conn);
		this.scheduler = new BatchScheduler(conn, new MartDeployer(this.conn));

		File logDir = new File(JOBHISTORYPATH);
		if (!logDir.exists()) {
			LOG.info("Creating Wocci Job History Directory.");
			logDir.mkdirs();
		}
	}

	protected AbsPerformer(Connector conn, ArchitectureScheduler scheduler, WorkflowEnactor enactor) {
		this.conn = conn;
		this.scheduler = scheduler;
		this.enactor = enactor;

		File logDir = new File(JOBHISTORYPATH);
		if (!logDir.exists()) {
			LOG.info("Creating Wocci Job History Directory.");
			logDir.mkdirs();
		}
	}

	public void performWorkflow(Path designTimePath) {
		org.eclipse.emf.ecore.resource.Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designTimePath);
		performWorkflow(designTimeModel);
	}

	public void performWorkflow(Resource designTimeModel) {
		Date startDate = new Date();
		String jobName = Paths.get(designTimeModel.getURI().path()).getFileName().toString();
		jobName = Files.getNameWithoutExtension(jobName);
		String jobId = "job_" + startDate.getTime();
		String jobFolder = createJobFolder(jobId);
		this.scheduler.setJobHistoryPath(jobFolder + "/scheduler/");
		this.enactor.setJobHistoryPath(jobFolder + "/enactor/");

		String status = "error";
		try {
			startWorkflow(designTimeModel);
		} catch (Exception e) {
			LOG.fatal("An error occured during workflow execution!");
			e.printStackTrace();
			status = "error";
		} finally {
			Date endDate = new Date();

			Resource runtimeModel = PerformerUtility.updatedRuntimeModel(conn, runtimePath);
			if (WorkflowUtility.tasksFinished(runtimeModel)) {
				status = "success";
			} else if (flag == false) {
				status = "stopped";
			}

			createHistoryFile(jobName, jobId, startDate, endDate, jobFolder, status);

			LOG.info("Finished Workflow Execution!");
		}
	}

	private String createJobFolder(String jobId) {
		String jobPath = JOBHISTORYPATH + jobId + "/";
		LOG.info(jobPath);
		File logDir = new File(jobPath);
		if (!logDir.exists()) {
			LOG.info("Creating log Directory.");
			logDir.mkdirs();
		}
		return jobPath;
	}

	private void createHistoryFile(String jobName, String jobId, Date startDate, Date endDate, String jobFolder,
			String status) {
		WocciHistory hist = new WocciHistory(jobName, jobId, startDate, endDate, this, status);
		hist.storeHistory(jobFolder);
	}

	protected org.eclipse.emf.ecore.resource.Resource updatedRuntimeModel() {
		Resource runtimeModel = null;
		boolean success = false;
		int retries = 0;
		while (success == false && retries < 3) {
			try {
				CachedResourceSet.getCache().clear();
				conn.loadRuntimeModel(reqRuntimePath);
				runtimeModel = de.ugoe.cs.rwm.docci.ModelUtility.loadOCCIintoEMFResource(reqRuntimePath);
				success = true;
			} catch (RuntimeException e) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				LOG.info("Downloading runtime model failed! Retrying!");
				retries++;
			}
		}
		return runtimeModel;
	}

	protected void enactWorkflowTasks(Resource runtimeModel) {
		try {
			enactor.enactWorkflow(runtimeModel);
		} catch (RuntimeException e) {
			LOG.info("Task enactment failed! Skipping cycle!");
			printConfig(runtimeModel);
			e.printStackTrace();
		}
	}

	protected synchronized void scheduleArchitecture(Resource designTimeModel, Resource runtimeModel) {
		try {
			scheduler.scheduleArchitecture(designTimeModel, runtimeModel);
		} catch (ArchitectureSchedulingException e1) {
			LOG.info("Scheduling Failed! Skipping Cycle!");
			printConfig(runtimeModel);
			e1.printStackTrace();
		}
	}

	private void printConfig(Resource runtimeModel) {
		for (TreeIterator<EObject> it = runtimeModel.getAllContents(); it.hasNext();) {
			EObject obj = it.next();
			if (obj instanceof org.eclipse.cmf.occi.core.Resource) {
				org.eclipse.cmf.occi.core.Resource r = (org.eclipse.cmf.occi.core.Resource) obj;
				LOG.info(r.getTitle() + ": " + r.getId());
				for (Link l : r.getLinks()) {
					LOG.info("		" + l);
				}
			}
		}
	}

	public ArchitectureScheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(ArchitectureScheduler scheduler) {
		this.scheduler = scheduler;
	}

	public WorkflowEnactor getEnactor() {
		return enactor;
	}

	public void setEnactor(WorkflowEnactor enactor) {
		this.enactor = enactor;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public Connector getConnector() {
		return this.conn;
	}
}
