package de.ugoe.cs.rwm.wocci.performer;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import org.eclipse.emf.ecore.resource.Resource;

import java.nio.file.Path;

public interface Performer {
    public void performWorkflow(Resource designTimeModel);
    public void performWorkflow(Path designTimeModelPath);
    public void setFlag(Boolean flag);
    public Connector getConnector();
    public ArchitectureScheduler getScheduler();
    public WorkflowEnactor getEnactor();
}
