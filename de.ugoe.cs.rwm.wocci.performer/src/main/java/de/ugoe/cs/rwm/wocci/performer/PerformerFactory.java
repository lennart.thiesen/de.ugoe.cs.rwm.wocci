package de.ugoe.cs.rwm.wocci.performer;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;

public class PerformerFactory {

    public static Performer getPerformer(Connector conn, ArchitectureScheduler sch, WorkflowEnactor enact, String performer){

        switch (performer) {
            case "Sequential":
                return new SequentialPerformer(conn, sch, enact);
            case "Parallel":
                return new ParallelPerformer(conn, sch, enact);
            case "Alternating":
                return new AlternatingPerformer(conn, sch, enact);
            default:
                return new SequentialPerformer(conn, sch, enact);
        }

    }
}
