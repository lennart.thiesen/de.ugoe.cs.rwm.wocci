/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.performer.sequential;

import static org.junit.Assert.assertEquals;

import org.eclipse.cmf.occi.core.Configuration;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.DynamicEnactor;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.performer.SequentialPerformer;
import de.ugoe.cs.rwm.wocci.performer.TestUtility;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.DiscreteScheduler;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class SequentialPerformerHadoopTest {
	private static SequentialPerformer performer;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestExtensionRegistry.extensionRegistrySetup();

		Connector conn = TestUtility.CONN;
		Deployer depl = new MartDeployer(conn, 1000);
		ArchitectureScheduler scheduler = new DiscreteScheduler(conn, depl);
		WorkflowEnactor enactor = new DynamicEnactor(conn);

		performer = new SequentialPerformer(conn, scheduler, enactor);
	}

	@Before
	public void deprovisionEverything() {
		TestUtility.deprovisionEverything();
	}

	@Test
	public void hadoopDistrFetcherTest() {
		TestUtility.performWorkflow(performer, "occi/hadoop/wordcount_workflow_distr_fetcher.occic");
		Configuration config = TestUtility.loadConfiguration(TestUtility.CONN);
		assertEquals(3, TestUtility.getFinishedTasks(config));
		assertEquals(0, TestUtility.getSkippedTasks(config));
	}

	@Test
	public void hadoopSize3Test() {
		TestUtility.performWorkflow(performer, "occi/hadoop/wordcount_workflow_size_3.occic");
		Configuration config = TestUtility.loadConfiguration(TestUtility.CONN);
		assertEquals(3, TestUtility.getFinishedTasks(config));
		assertEquals(0, TestUtility.getSkippedTasks(config));
	}
}
