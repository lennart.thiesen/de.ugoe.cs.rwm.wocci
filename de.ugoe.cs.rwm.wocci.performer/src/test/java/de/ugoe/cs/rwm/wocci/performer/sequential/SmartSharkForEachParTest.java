/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.performer.sequential;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;
import de.ugoe.cs.rwm.tocci.occi2openstack.OCCI2OPENSTACKTransformator;
import de.ugoe.cs.rwm.wocci.enactor.DynamicEnactor;
import de.ugoe.cs.rwm.wocci.performer.SequentialPerformer;
import de.ugoe.cs.rwm.wocci.performer.TestUtility;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.SchedulerFactory;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class SmartSharkForEachParTest {
	String manNWid = "urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590";
	String managementNWRuntimeId = "75a4639e-9ce7-4058-b859-8a711b0e2e7b";
	String sshKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6H7Ydi45BTHid4qNppGAi5mzjbnZgt7bi6xLGmZG9CiLmhMsxOuk3Z05Nn+pmoN98qS0eY8S240PPk5VOlYqBY0vdRAwrZSHHaLdMp6I7ARNrI2KraYduweqz7ZQxPXQfwIeYx2HKQxEF2r+4//Fo4WfgdBkLuulvl/Gw3TUzJNQHvgpaiNo9+PI5CZydHnZbjUkRikS12pT+CbNKj+0QKeQztbCd41aKxDv5H0DjltVRcpPppv4dmiU/zoCAIngWLO1PPgfYWyze8Z9IoyBT7Qdg30U91TYZBuxzXR5lq7Fh64y/IZ/SjdOdSIvIuDjtmJDULRdLJzrvubrKY+YH Generated-by-Nova";
	String userData = "I2Nsb3VkLWNvbmZpZwoKIyBVcGdyYWRlIHRoZSBpbnN0YW5jZSBvbiBmaXJzdCBib290CiMgKGllIHJ1biBhcHQtZ2V0IHVwZ3JhZGUpCiMKIyBEZWZhdWx0OiBmYWxzZQojIEFsaWFzZXM6IGFwdF91cGdyYWRlCnBhY2thZ2VfdXBncmFkZTogdHJ1ZQoKcGFja2FnZXM6CiAtIHB5dGhvbgoKd3JpdGVfZmlsZXM6CiAgLSBwYXRoOiAvZXRjL25ldHdvcmsvaW50ZXJmYWNlcy5kLzUwLWNsb3VkLWluaXQuY2ZnCiAgICBjb250ZW50OiB8CiAgICAgIGF1dG8gbG8KICAgICAgaWZhY2UgbG8gaW5ldCBsb29wYmFjawogICAgICAKICAgICAgYXV0byBlbnMwCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMAogICAgICBpZmFjZSBlbnMwIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMQogICAgICBpZmFjZSBlbnMxIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMyCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMgogICAgICBpZmFjZSBlbnMyIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMzCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMwogICAgICBpZmFjZSBlbnMzIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM0CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNAogICAgICBpZmFjZSBlbnM0IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM1CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNQogICAgICBpZmFjZSBlbnM1IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM2CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNgogICAgICBpZmFjZSBlbnM2IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM3CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNwogICAgICBpZmFjZSBlbnM3IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM4CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOAogICAgICBpZmFjZSBlbnM4IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM5CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOQogICAgICBpZmFjZSBlbnM5IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxMAogICAgICBhbGxvdy1ob3RwbHVnIGVuczEwCiAgICAgIGlmYWNlIGVuczEwIGluZXQgZGhjcAoKIyMj";
	String flavor = "ce8c33af-0cd5-4aac-b6f3-fcde58c4b262";
	String image = "e02f6965-0c9e-45e0-9a54-e2730bd05749";
	String remoteUser = "ubuntu";

	OCCI2OPENSTACKTransformator otrans = new OCCI2OPENSTACKTransformator();

	Path runtimeOCCI = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
	private static SequentialPerformer performer;
	private static Connector conn = TestUtility.CONN;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestExtensionRegistry.extensionRegistrySetup();

		MartDeployer deployer = new MartDeployer(conn);
		ArchitectureScheduler scheduler = SchedulerFactory.getArchScheduler("Discrete", conn, deployer);
		DynamicEnactor enactor = new DynamicEnactor(conn);
		performer = new SequentialPerformer(conn, scheduler, enactor);
	}

	@Before
	public void deprovisionEverything() {
		TestUtility.deprovisionEverything();
	}

	@Test
	public void foreachSmartSharkParShared() throws EolRuntimeException {
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/SmartSharkForEachLocalParShared.occic"));
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);
		otrans.setTransformationProperties(managementNWRuntimeId, sshKey, userData, manNWid, flavor, image, remoteUser);
		otrans.transform(occiPath, occiPath);

		TestUtility.performWorkflow(performer, "occi/SmartSharkForEachLocalParShared.occic");

		conn.loadRuntimeModel(runtimeOCCI);
		Configuration config = ModelUtility.loadOCCIConfiguration(runtimeOCCI);
		TestUtility.checkLoopIterationCount(config, 3, 4);
		TestUtility.checkAllTasksFinsihed(config);
	}

	@Test
	public void foreachSmartSharkPar3Shared() throws EolRuntimeException {
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/SmartSharkForEachLocalPar3Shared.occic"));
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);
		otrans.setTransformationProperties(managementNWRuntimeId, sshKey, userData, manNWid, flavor, image, remoteUser);
		otrans.transform(occiPath, occiPath);

		TestUtility.performWorkflow(performer, "occi/SmartSharkForEachLocalPar3Shared.occic");

		conn.loadRuntimeModel(runtimeOCCI);
		Configuration config = ModelUtility.loadOCCIConfiguration(runtimeOCCI);
		TestUtility.checkLoopIterationCount(config, 1, 3);
		// Sometimes in this testcase the original mecoshark is skipped
		// Therefore, the checkalltasksfinished assert is ommited.
		TestUtility.checkAllTasksFinsihed(config);
	}
}
