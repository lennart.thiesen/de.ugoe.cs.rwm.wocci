/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.utility;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.ComputeStatus;
import org.eclipse.cmf.occi.infrastructure.Network;
import org.eclipse.cmf.occi.infrastructure.Storage;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.modmacao.occi.platform.Application;
import org.modmacao.occi.platform.Component;
import org.modmacao.occi.platform.Componentlink;
import org.modmacao.placement.Placementlink;

import workflow.*;

public class WorkflowUtility {
	static final Logger LOG = Logger.getLogger(WorkflowUtility.class.getName());

	public static EList<Link> getDatalinskForExecution(org.eclipse.emf.ecore.resource.Resource runtimeModel) {
		EList<Link> datalinksToBeExecuted = new BasicEList<Link>();
		for (org.eclipse.cmf.occi.core.Link dLink : ModelUtility.getDatalinks(runtimeModel)) {
			if (isReadyForExecution(dLink)) {
				LOG.debug("Datalink ready for execution: " + dLink.getTitle());
				datalinksToBeExecuted.add(dLink);
			}
		}
		return datalinksToBeExecuted;
	}

	public static boolean containsErrors(org.eclipse.emf.ecore.resource.Resource runtimeModel) {
		Configuration config = de.ugoe.cs.rwm.docci.ModelUtility.getOCCIConfiguration(runtimeModel);
		boolean error = false;
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res instanceof Application) {
				Application app = (Application) res;
				if (app.getOcciAppState().getValue() == org.modmacao.occi.platform.Status.ERROR_VALUE) {
					error = true;
				}
			} else if (res instanceof Component) {
				Component app = (Component) res;
				if (app.getOcciComponentState().getValue() == org.modmacao.occi.platform.Status.ERROR_VALUE) {
					error = true;
				}
			} else if (res instanceof Compute) {
				Compute comp = (Compute) res;
				if (comp.getOcciComputeState()
						.getValue() == org.eclipse.cmf.occi.infrastructure.ComputeStatus.ERROR_VALUE) {
					error = true;
				}
			} else if (res instanceof Storage) {
				Storage stor = (Storage) res;
				if (stor.getOcciStorageState()
						.getValue() == org.eclipse.cmf.occi.infrastructure.StorageStatus.ERROR_VALUE) {
					error = true;
				}
			} else if (res instanceof Network) {
				Network nw = (Network) res;
				if (nw.getOcciNetworkState()
						.getValue() == org.eclipse.cmf.occi.infrastructure.NetworkStatus.ERROR_VALUE) {
					error = true;
				}
			}
		}
		if (error) {
			LOG.info("Aborting Workflow execution. A deployed Component reached the Error State.");
		}
		return error;
	}

	private static boolean isReadyForExecution(Link dLink) {
		boolean taskDependenciesFulfilled = isTaskDependencyFulfilled(dLink);
		boolean platformDependenciesFulfilled = isPlatformDependencyFulfilled(dLink);
		boolean scheduled = isScheduled((Datalink) dLink);
		LOG.debug("Task: " + dLink.getTitle() + " | Platformdependency: " + platformDependenciesFulfilled
				+ " | TaskDependency: " + taskDependenciesFulfilled);
		if (scheduled && taskDependenciesFulfilled && platformDependenciesFulfilled) {
			return true;
		}

		return false;
	}

	private static boolean isPlatformDependencyFulfilled(Link dLink) {
		Task srcTask = (Task) dLink.getSource();
		Task tarTask = (Task) dLink.getTarget();

		Compute srcHost = getHost(srcTask);
		Compute tarHost = getHost(tarTask);

		if (srcHost == null || tarHost == null) {
			return false;
		}

		// Remotedatacanal rmdCanal = getRemotedatacanal(dLink);
		LOG.info("Improve to support Remotedatacanal");

		if (srcHost.getOcciComputeState().getValue() == org.eclipse.cmf.occi.infrastructure.ComputeStatus.ACTIVE
				.getValue()
				&& tarHost.getOcciComputeState().getValue() == org.eclipse.cmf.occi.infrastructure.ComputeStatus.ACTIVE
						.getValue()) {
			return true;
		}

		return false;
	}

	private static Remotedatacanal getRemotedatacanal(Link dLink) {
		for (MixinBase mixin : dLink.getParts()) {
			if (mixin instanceof Remotedatacanal) {
				return (Remotedatacanal) mixin;
			}
		}
		return null;
	}

	private static Compute getHost(Task task) {
		for (Link execLink : task.getLinks()) {
			if (execLink instanceof Executionlink) {
				Component comp = (Component) execLink.getTarget();
				for (Link placLink : comp.getLinks()) {
					if (placLink instanceof Placementlink) {
						return (Compute) placLink.getTarget();
					}
				}
			}
		}
		return null;
		// throw new NoSuchElementException("No hosting compute found.");
	}

	private static boolean isScheduled(Datalink dLink) {
		if (dLink.getTaskDatalinkState().getValue() == Status.SCHEDULED.getValue()) {
			return true;
		}
		return false;
	}

	private static boolean isTaskDependencyFulfilled(Link dLink) {
		Task srcTask = (Task) dLink.getSource();
		if (srcTask.getWorkflowTaskState().getValue() == Status.FINISHED.getValue()) {
			return true;
		}
		return false;
	}

	private static List<Task> getFollowingTasks(Task task) {
		List<Task> followingTasks = new ArrayList<Task>();
		for (Taskdependency dep : getTaskdependencies(task)) {
			followingTasks.add((Task) dep.getTarget());
		}
		return followingTasks;
	}

	private static List<Taskdependency> getTaskdependencies(Task task) {
		List<Taskdependency> cLinks = new ArrayList<Taskdependency>();
		for (Link link : task.getLinks()) {

			if (link instanceof Taskdependency) {
				cLinks.add((Taskdependency) link);
			}
		}
		return cLinks;
	}

	public static boolean hasDecisionInput(Decision decision) {
		if (decision.getWorkflowDecisionInput().equals("") == false) {
			return true;
		} else {
			for (AttributeState attr : decision.getAttributes()) {
				if (attr.getName().equals("workflow.decision.input")) {
					if (attr.getValue().equals("") == false) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean hasConnectionToPlatform(Resource task) {
		if (ModelUtility.getPlatformDependencyLinks(task.getLinks()).isEmpty()
				&& ModelUtility.getExecutionLinks(task.getLinks()).isEmpty()) {
			return false;
		}
		return true;
	}

	public static EList<Resource> getTasksOnlyRequiringPlatform(org.eclipse.emf.ecore.resource.Resource runtimeModel) {
		EList<Resource> tasksToBeExecuted = new BasicEList<Resource>();
		for (org.eclipse.cmf.occi.core.Resource task : ModelUtility.getTasksAsResource(runtimeModel)) {
			if (onlyRequiresPlatform(task)) {
				LOG.debug("Task ready for execution: " + task.getTitle());
				tasksToBeExecuted.add(task);
			}
		}
		return tasksToBeExecuted;
	}

	private static boolean onlyRequiresPlatform(org.eclipse.cmf.occi.core.Resource task) {
		// boolean deployed = isDeployed(comp);
		boolean taskDependenciesFulfilled = isTaskDependencyFulfilled(task);
		boolean scheduled = isScheduled((Task) task);
		LOG.debug("Task: " + task.getTitle() + " | TaskDependency: " + taskDependenciesFulfilled);
		if (scheduled && taskDependenciesFulfilled) {
			return true;
		}

		return false;
	}

	public static boolean tasksFinished(org.eclipse.emf.ecore.resource.Resource runtimeModel) {
		EList<Task> tasks = ModelUtility.getTasks(runtimeModel);
		for (Task task : tasks) {
			if (isFinished(task) == false && isError(task) == false && cantBeReached(task) == false
					&& isSkipped(task) == false) {
				return false;
			}
		}
		LOG.info("Each Task is finished!");
		LOG.debug(runtimeModel);
		return true;
	}

	private static boolean isSkipped(Task task) {
		if (task.getWorkflowTaskState().getValue() == Status.SKIPPED.getValue()) {
			return true;
		}
		return false;
	}

	private static boolean cantBeReached(Task task) {
		for (Link link : task.getRlinks()) {
			if (link instanceof Datalink) {
				Datalink dl = (Datalink) link;
				if (dl.getTaskDatalinkState().getValue() == workflow.Status.ERROR.getValue()) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean isError(Task task) {
		if (task.getWorkflowTaskState().getValue() == Status.ERROR.getValue()) {
			return true;
		}
		return false;
	}

	private static boolean isFinished(Task task) {
		if (task.getWorkflowTaskState().getValue() == Status.FINISHED.getValue()) {
			return true;
		}
		return false;
	}

	public static boolean isScheduled(Task task) {
		if (task.getWorkflowTaskState().getValue() == Status.SCHEDULED.getValue()) {
			return true;
		}
		return false;
	}

	public static boolean isTaskDependencyFulfilled(Resource task) {
		boolean fulfilled = true;
		for (Taskdependency tDependency : ModelUtility.getTaskDependencyLinks(task.getRlinks())) {
			Task sourceTask = (Task) tDependency.getSource();
			if (tDependency.getTaskDependencyIsloose() == false) {
				fulfilled = determineTaskDependencyFulfillment((Task) task, sourceTask, tDependency);
			} else {
				fulfilled = determineLooseTaskDependencyFulfillment(task, sourceTask, tDependency);
			}
			if (fulfilled == false) {
				return false;
			}
		}
		return true;

	}

	private static boolean determineTaskDependencyFulfillment(Task task, Task sourceTask, Taskdependency tDependency) {
		if (task instanceof Loop) {
			if (buildsLoop(sourceTask, task)) {
				return true;
			}
		}

		if (sourceTask instanceof Loop) {
			if (sourceTask.getWorkflowTaskState().getValue() != Status.FINISHED.getValue()
					&& sourceTask.getWorkflowTaskState().getValue() != Status.SKIPPED.getValue()
					&& sourceTask.getWorkflowTaskState().getValue() != Status.ACTIVE_VALUE) {
				return false;
			}
		} else {
			if (sourceTask.getWorkflowTaskState().getValue() != Status.FINISHED.getValue()
					&& sourceTask.getWorkflowTaskState().getValue() != Status.SKIPPED.getValue()) {
				return false;
			}
			if (tDependency instanceof Datalink) {
				Datalink dlink = (Datalink) tDependency;
				if (dlink.getTaskDatalinkState().getValue() != Status.FINISHED.getValue()
						&& sourceTask.getWorkflowTaskState().getValue() != Status.SKIPPED.getValue()) {
					return false;
				}
			}
		}
		return true;
	}

	public static boolean buildsLoop(Task sourceTask, Task loop) {
		HashSet checked = new HashSet();
		return buildsLoop(sourceTask, loop, checked);
	}

	private static boolean buildsLoop(Task sourceTask, Task loop, HashSet checked) {
		if (checked.contains(sourceTask.getId())) {
			return false;
		}
		checked.add(sourceTask.getId());
		for (Task pTask : getPreviousTasks(sourceTask)) {
			if (pTask.getId().equals(loop.getId())) {
				return true;
			} else if (pTask.getId().equals(sourceTask.getId()) == false) {
				return buildsLoop(pTask, loop, checked);
			}
		}
		return false;
	}

	public static boolean buildsLoopForward(Task tarTask, Task loop) {
		for (Task pTask : getFollowingTasks(tarTask)) {
			if (pTask.equals(loop)) {
				return true;
			} else {
				buildsLoopForward(pTask, loop);
			}
		}
		return false;
	}

	public static List<Task> getLoopedTasks(Loop loop) {
		EList<Task> loopedTasks = new BasicEList<>();
		for (Taskdependency tdep : ModelUtility.getTaskDependencyLinks(loop.getRlinks())) {
			Task srcTask = (Task) tdep.getSource();
			if (buildsLoop(srcTask, loop)) {
				loopedTasks.add(srcTask);
			}
		}
		return loopedTasks;
	}

	private static BasicEList<Task> getPreviousTasks(Task sourceTask) {
		BasicEList<Task> tasks = new BasicEList<Task>();
		for (Taskdependency tdep : ModelUtility.getTaskDependencyLinks(sourceTask.getRlinks())) {
			Task task = (Task) tdep.getSource();
			tasks.add(task);
		}
		return tasks;
	}

	private static Boolean determineLooseTaskDependencyFulfillment(Resource task, Task sourceTask,
			Taskdependency tDependency) {
		if (task instanceof Loop) {
			if (sourceTask.getWorkflowTaskState().getValue() != Status.SCHEDULED.getValue()) {
				return false;
			}
		} else if (sourceTask.getWorkflowTaskState().getValue() != Status.ACTIVE.getValue()
				&& sourceTask.getWorkflowTaskState().getValue() != Status.FINISHED.getValue()
				&& sourceTask.getWorkflowTaskState().getValue() != Status.SKIPPED.getValue()) {
			return false;
		}
		if (tDependency instanceof Datalink) {
			Datalink dlink = (Datalink) tDependency;
			if (dlink.getTaskDatalinkState().getValue() != Status.FINISHED.getValue()
					&& dlink.getTaskDatalinkState().getValue() != Status.ACTIVE.getValue()
					&& sourceTask.getWorkflowTaskState().getValue() != Status.SKIPPED.getValue()) {
				return false;
			}
		}
		return true;
	}

	public static boolean isPlatformDependencyFulfilled(Resource task) {
		EList<Link> dependencies = ModelUtility.getPlatformDependencyLinks(task.getLinks());
		for (Link dependency : dependencies) {
			Platformdependency pDependency = (Platformdependency) dependency;
			if (pDependency.getTarget() instanceof Component) {
				Component reqComponent = (Component) pDependency.getTarget();
				if (reqComponent.getOcciComponentState().getValue() != org.modmacao.occi.platform.Status.ACTIVE
						.getValue()) {
					return false;
				}
			} else if (pDependency.getTarget() instanceof Application) {
				Application reqApplication = (Application) pDependency.getTarget();
				if (reqApplication.getOcciAppState().getValue() != org.modmacao.occi.platform.Status.ACTIVE
						.getValue()) {
					if (isAlreadyPerformedDecisionExecutable(reqApplication) == false) {
						return false;
					}
				}
			} else if (pDependency.getTarget() instanceof Compute) {
				Compute reqComp = (Compute) pDependency.getTarget();
				if (reqComp.getOcciComputeState().getValue() != ComputeStatus.ACTIVE_VALUE) {
					return false;
				}
			}
		}
		return true;

	}

	private static boolean isAlreadyPerformedDecisionExecutable(Application reqApplication) {
		if (reqApplication.getOcciAppState().getLiteral().equals("inactive")) {
			for (Link rLink : reqApplication.getRlinks()) {
				if (rLink instanceof Executionlink) {
					Executionlink eLink = (Executionlink) rLink;
					if (eLink.getSource() instanceof Decision) {
						Decision dec = (Decision) eLink.getSource();
						if (dec.getWorkflowTaskState().getValue() == Status.FINISHED.getValue()) {
							return true;
						}
						if (dec instanceof Loop) {
							Loop loop = (Loop) dec;
							if (loop.getWorkflowTaskState().getValue() == Status.FINISHED.getValue()
									|| loop.getWorkflowTaskState().getValue() == Status.ACTIVE.getValue()) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	private static boolean isDeployed(Component comp) {
		Application app = ModelUtility.getComponentApplication(comp);
		if (app == null) {
			LOG.debug("Component is not contained within an Application");
			if (comp.getOcciComponentState().getValue() == org.modmacao.occi.platform.Status.DEPLOYED.getValue()) {
				LOG.debug("Component: " + comp.getTitle() + " in Status: " + comp.getOcciComponentState());
				return true;
			}
		} else {
			LOG.debug("Application of Component: " + comp.getTitle() + " in Status: " + app.getOcciAppState());
			if (app.getOcciAppState().getValue() == org.modmacao.occi.platform.Status.DEPLOYED.getValue()) {
				return true;
			}
		}
		return false;
	}

	private static boolean isWorkflowFinished(org.eclipse.emf.ecore.resource.Resource model) {
		boolean everyTaskFinished = true;
		Configuration config = (Configuration) model.getContents().get(0);
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res instanceof Task) {
				Task task = (Task) res;
				if ((task.getWorkflowTaskState().getValue() == Status.FINISHED.getValue()) == false
						&& task.getWorkflowTaskState().getValue() == Status.SKIPPED.getValue() == false) {
					everyTaskFinished = false;
					System.out.println("Task not finished: " + task);
				}
			}
		}

		if (everyTaskFinished) {
			return true;
		}

		return false;
	}

	public static boolean isExecutableHostRunning(Resource task) {
		for (Link elink : task.getLinks()) {
			if (elink instanceof Executionlink) {
				if (elink.getTarget() instanceof Component) {
					return isHostComputeActive(elink.getTarget());
				} else if (elink.getTarget() instanceof Application) {
					Application app = (Application) elink.getTarget();
					// Could be further improved by checking the componets against
					// the design time model
					if (getComponents(app).size() == 0) {
						return false;
					}
					for (Component comp : getComponents(app)) {
						if (isHostComputeActive(comp) == false) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	private static List<Component> getComponents(Application app) {
		List<Component> comps = new ArrayList<>();
		for (Link clink : app.getLinks()) {
			if (clink instanceof Componentlink) {
				if (clink.getTarget() instanceof Component) {
					comps.add((Component) clink.getTarget());
				}
			}
		}
		return comps;
	}

	private static boolean isHostComputeActive(Resource target) {
		for (Link plink : target.getLinks()) {
			if (plink instanceof Placementlink) {
				if (plink.getTarget() != null && plink.getTarget() instanceof Compute) {
					Compute comp = (Compute) plink.getTarget();
					if (comp.getOcciComputeState().getValue() == ComputeStatus.ACTIVE_VALUE) {
						return true;
					} else {
						return false;
					}
				} else {
					LOG.error("Target of placementlink is no Compute node: " + plink.getTitle() + "(" + plink.getId()
							+ ")" + "| tar: " + plink.getTarget() + "| src: " + plink.getSource());
				}
			}
		}
		return false;
	}
}
