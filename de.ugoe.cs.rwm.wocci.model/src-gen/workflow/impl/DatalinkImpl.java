/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import workflow.Datalink;
import workflow.Status;
import workflow.WorkflowPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Datalink</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link workflow.impl.DatalinkImpl#getTaskDatalinkSourceFile <em>Task Datalink Source File</em>}</li>
 *   <li>{@link workflow.impl.DatalinkImpl#getTaskDatalinkTargetFile <em>Task Datalink Target File</em>}</li>
 *   <li>{@link workflow.impl.DatalinkImpl#getTaskDatalinkState <em>Task Datalink State</em>}</li>
 *   <li>{@link workflow.impl.DatalinkImpl#getTaskDatalinkStateMessage <em>Task Datalink State Message</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DatalinkImpl extends TaskdependencyImpl implements Datalink {
	/**
	 * The default value of the '{@link #getTaskDatalinkSourceFile() <em>Task Datalink Source File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDatalinkSourceFile()
	 * @generated
	 * @ordered
	 */
	protected static final String TASK_DATALINK_SOURCE_FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTaskDatalinkSourceFile() <em>Task Datalink Source File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDatalinkSourceFile()
	 * @generated
	 * @ordered
	 */
	protected String taskDatalinkSourceFile = TASK_DATALINK_SOURCE_FILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTaskDatalinkTargetFile() <em>Task Datalink Target File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDatalinkTargetFile()
	 * @generated
	 * @ordered
	 */
	protected static final String TASK_DATALINK_TARGET_FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTaskDatalinkTargetFile() <em>Task Datalink Target File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDatalinkTargetFile()
	 * @generated
	 * @ordered
	 */
	protected String taskDatalinkTargetFile = TASK_DATALINK_TARGET_FILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTaskDatalinkState() <em>Task Datalink State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDatalinkState()
	 * @generated
	 * @ordered
	 */
	protected static final Status TASK_DATALINK_STATE_EDEFAULT = Status.SCHEDULED;

	/**
	 * The cached value of the '{@link #getTaskDatalinkState() <em>Task Datalink State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDatalinkState()
	 * @generated
	 * @ordered
	 */
	protected Status taskDatalinkState = TASK_DATALINK_STATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTaskDatalinkStateMessage() <em>Task Datalink State Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDatalinkStateMessage()
	 * @generated
	 * @ordered
	 */
	protected static final String TASK_DATALINK_STATE_MESSAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTaskDatalinkStateMessage() <em>Task Datalink State Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDatalinkStateMessage()
	 * @generated
	 * @ordered
	 */
	protected String taskDatalinkStateMessage = TASK_DATALINK_STATE_MESSAGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DatalinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowPackage.Literals.DATALINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTaskDatalinkSourceFile() {
		return taskDatalinkSourceFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTaskDatalinkSourceFile(String newTaskDatalinkSourceFile) {
		String oldTaskDatalinkSourceFile = taskDatalinkSourceFile;
		taskDatalinkSourceFile = newTaskDatalinkSourceFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.DATALINK__TASK_DATALINK_SOURCE_FILE, oldTaskDatalinkSourceFile, taskDatalinkSourceFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTaskDatalinkTargetFile() {
		return taskDatalinkTargetFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTaskDatalinkTargetFile(String newTaskDatalinkTargetFile) {
		String oldTaskDatalinkTargetFile = taskDatalinkTargetFile;
		taskDatalinkTargetFile = newTaskDatalinkTargetFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.DATALINK__TASK_DATALINK_TARGET_FILE, oldTaskDatalinkTargetFile, taskDatalinkTargetFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Status getTaskDatalinkState() {
		return taskDatalinkState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTaskDatalinkState(Status newTaskDatalinkState) {
		Status oldTaskDatalinkState = taskDatalinkState;
		taskDatalinkState = newTaskDatalinkState == null ? TASK_DATALINK_STATE_EDEFAULT : newTaskDatalinkState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.DATALINK__TASK_DATALINK_STATE, oldTaskDatalinkState, taskDatalinkState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTaskDatalinkStateMessage() {
		return taskDatalinkStateMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTaskDatalinkStateMessage(String newTaskDatalinkStateMessage) {
		String oldTaskDatalinkStateMessage = taskDatalinkStateMessage;
		taskDatalinkStateMessage = newTaskDatalinkStateMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.DATALINK__TASK_DATALINK_STATE_MESSAGE, oldTaskDatalinkStateMessage, taskDatalinkStateMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void start() {
		throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!start()
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void stop() {
		throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!stop()
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void schedule() {
		throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!schedule()
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void skip() {
		throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!skip()
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowPackage.DATALINK__TASK_DATALINK_SOURCE_FILE:
				return getTaskDatalinkSourceFile();
			case WorkflowPackage.DATALINK__TASK_DATALINK_TARGET_FILE:
				return getTaskDatalinkTargetFile();
			case WorkflowPackage.DATALINK__TASK_DATALINK_STATE:
				return getTaskDatalinkState();
			case WorkflowPackage.DATALINK__TASK_DATALINK_STATE_MESSAGE:
				return getTaskDatalinkStateMessage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowPackage.DATALINK__TASK_DATALINK_SOURCE_FILE:
				setTaskDatalinkSourceFile((String)newValue);
				return;
			case WorkflowPackage.DATALINK__TASK_DATALINK_TARGET_FILE:
				setTaskDatalinkTargetFile((String)newValue);
				return;
			case WorkflowPackage.DATALINK__TASK_DATALINK_STATE:
				setTaskDatalinkState((Status)newValue);
				return;
			case WorkflowPackage.DATALINK__TASK_DATALINK_STATE_MESSAGE:
				setTaskDatalinkStateMessage((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowPackage.DATALINK__TASK_DATALINK_SOURCE_FILE:
				setTaskDatalinkSourceFile(TASK_DATALINK_SOURCE_FILE_EDEFAULT);
				return;
			case WorkflowPackage.DATALINK__TASK_DATALINK_TARGET_FILE:
				setTaskDatalinkTargetFile(TASK_DATALINK_TARGET_FILE_EDEFAULT);
				return;
			case WorkflowPackage.DATALINK__TASK_DATALINK_STATE:
				setTaskDatalinkState(TASK_DATALINK_STATE_EDEFAULT);
				return;
			case WorkflowPackage.DATALINK__TASK_DATALINK_STATE_MESSAGE:
				setTaskDatalinkStateMessage(TASK_DATALINK_STATE_MESSAGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowPackage.DATALINK__TASK_DATALINK_SOURCE_FILE:
				return TASK_DATALINK_SOURCE_FILE_EDEFAULT == null ? taskDatalinkSourceFile != null : !TASK_DATALINK_SOURCE_FILE_EDEFAULT.equals(taskDatalinkSourceFile);
			case WorkflowPackage.DATALINK__TASK_DATALINK_TARGET_FILE:
				return TASK_DATALINK_TARGET_FILE_EDEFAULT == null ? taskDatalinkTargetFile != null : !TASK_DATALINK_TARGET_FILE_EDEFAULT.equals(taskDatalinkTargetFile);
			case WorkflowPackage.DATALINK__TASK_DATALINK_STATE:
				return taskDatalinkState != TASK_DATALINK_STATE_EDEFAULT;
			case WorkflowPackage.DATALINK__TASK_DATALINK_STATE_MESSAGE:
				return TASK_DATALINK_STATE_MESSAGE_EDEFAULT == null ? taskDatalinkStateMessage != null : !TASK_DATALINK_STATE_MESSAGE_EDEFAULT.equals(taskDatalinkStateMessage);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorkflowPackage.DATALINK___START:
				start();
				return null;
			case WorkflowPackage.DATALINK___STOP:
				stop();
				return null;
			case WorkflowPackage.DATALINK___SCHEDULE:
				schedule();
				return null;
			case WorkflowPackage.DATALINK___SKIP:
				skip();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (taskDatalinkSourceFile: ");
		result.append(taskDatalinkSourceFile);
		result.append(", taskDatalinkTargetFile: ");
		result.append(taskDatalinkTargetFile);
		result.append(", taskDatalinkState: ");
		result.append(taskDatalinkState);
		result.append(", taskDatalinkStateMessage: ");
		result.append(taskDatalinkStateMessage);
		result.append(')');
		return result.toString();
	}

} //DatalinkImpl
