/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.cmf.occi.core.Entity;

import org.eclipse.cmf.occi.core.impl.MixinBaseImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.pivot.evaluation.Executor;

import org.eclipse.ocl.pivot.ids.IdResolver;
import org.eclipse.ocl.pivot.ids.TypeId;

import org.eclipse.ocl.pivot.internal.utilities.PivotUtilInternal;
import org.eclipse.ocl.pivot.library.oclany.OclAnyOclIsKindOfOperation;
import org.eclipse.ocl.pivot.library.oclany.OclComparableLessThanEqualOperation;

import org.eclipse.ocl.pivot.library.string.CGStringGetSeverityOperation;
import org.eclipse.ocl.pivot.library.string.CGStringLogDiagnosticOperation;

import org.eclipse.ocl.pivot.utilities.PivotUtil;
import org.eclipse.ocl.pivot.utilities.ValueUtil;

import org.eclipse.ocl.pivot.values.IntegerValue;
import org.eclipse.ocl.pivot.values.InvalidValueException;

import workflow.Foreach;
import workflow.WorkflowPackage;
import workflow.WorkflowTables;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Foreach</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link workflow.impl.ForeachImpl#getLoopItemDelimiter <em>Loop Item Delimiter</em>}</li>
 *   <li>{@link workflow.impl.ForeachImpl#getLoopItemName <em>Loop Item Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ForeachImpl extends MixinBaseImpl implements Foreach {
	/**
	 * The default value of the '{@link #getLoopItemDelimiter() <em>Loop Item Delimiter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopItemDelimiter()
	 * @generated
	 * @ordered
	 */
	protected static final String LOOP_ITEM_DELIMITER_EDEFAULT = ",";

	/**
	 * The cached value of the '{@link #getLoopItemDelimiter() <em>Loop Item Delimiter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopItemDelimiter()
	 * @generated
	 * @ordered
	 */
	protected String loopItemDelimiter = LOOP_ITEM_DELIMITER_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoopItemName() <em>Loop Item Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopItemName()
	 * @generated
	 * @ordered
	 */
	protected static final String LOOP_ITEM_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoopItemName() <em>Loop Item Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopItemName()
	 * @generated
	 * @ordered
	 */
	protected String loopItemName = LOOP_ITEM_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ForeachImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowPackage.Literals.FOREACH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLoopItemDelimiter() {
		return loopItemDelimiter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoopItemDelimiter(String newLoopItemDelimiter) {
		String oldLoopItemDelimiter = loopItemDelimiter;
		loopItemDelimiter = newLoopItemDelimiter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.FOREACH__LOOP_ITEM_DELIMITER, oldLoopItemDelimiter, loopItemDelimiter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLoopItemName() {
		return loopItemName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoopItemName(String newLoopItemName) {
		String oldLoopItemName = loopItemName;
		loopItemName = newLoopItemName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.FOREACH__LOOP_ITEM_NAME, oldLoopItemName, loopItemName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean appliesConstraint(final DiagnosticChain diagnostics, final Map<Object, Object> context) {
		/**
		 *
		 * inv appliesConstraint:
		 *   let severity : Integer[1] = 'Foreach::appliesConstraint'.getSeverity()
		 *   in
		 *     if severity <= 0
		 *     then true
		 *     else
		 *       let result : Boolean[1] = self.entity.oclIsKindOf(Loop)
		 *       in
		 *         'Foreach::appliesConstraint'.logDiagnostic(self, null, diagnostics, context, null, severity, result, 0)
		 *     endif
		 */
		final /*@NonInvalid*/ Executor executor = PivotUtilInternal.getExecutor(this);
		final /*@NonInvalid*/ IdResolver idResolver = executor.getIdResolver();
		final /*@NonInvalid*/ IntegerValue severity_0 = CGStringGetSeverityOperation.INSTANCE.evaluate(executor, WorkflowTables.STR_Foreach_c_c_appliesConstraint);
		final /*@NonInvalid*/ boolean le = OclComparableLessThanEqualOperation.INSTANCE.evaluate(executor, severity_0, WorkflowTables.INT_0).booleanValue();
		/*@NonInvalid*/ boolean symbol_0;
		if (le) {
			symbol_0 = ValueUtil.TRUE_VALUE;
		}
		else {
			final /*@NonInvalid*/ org.eclipse.ocl.pivot.Class TYP_workflow_c_c_Loop = idResolver.getClass(WorkflowTables.CLSSid_Loop, null);
			final /*@NonInvalid*/ Entity entity = this.getEntity();
			final /*@NonInvalid*/ boolean result = OclAnyOclIsKindOfOperation.INSTANCE.evaluate(executor, entity, TYP_workflow_c_c_Loop).booleanValue();
			final /*@NonInvalid*/ boolean logDiagnostic = CGStringLogDiagnosticOperation.INSTANCE.evaluate(executor, TypeId.BOOLEAN, WorkflowTables.STR_Foreach_c_c_appliesConstraint, this, (Object)null, diagnostics, context, (Object)null, severity_0, result, WorkflowTables.INT_0).booleanValue();
			symbol_0 = logDiagnostic;
		}
		return Boolean.TRUE == symbol_0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowPackage.FOREACH__LOOP_ITEM_DELIMITER:
				return getLoopItemDelimiter();
			case WorkflowPackage.FOREACH__LOOP_ITEM_NAME:
				return getLoopItemName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowPackage.FOREACH__LOOP_ITEM_DELIMITER:
				setLoopItemDelimiter((String)newValue);
				return;
			case WorkflowPackage.FOREACH__LOOP_ITEM_NAME:
				setLoopItemName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowPackage.FOREACH__LOOP_ITEM_DELIMITER:
				setLoopItemDelimiter(LOOP_ITEM_DELIMITER_EDEFAULT);
				return;
			case WorkflowPackage.FOREACH__LOOP_ITEM_NAME:
				setLoopItemName(LOOP_ITEM_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowPackage.FOREACH__LOOP_ITEM_DELIMITER:
				return LOOP_ITEM_DELIMITER_EDEFAULT == null ? loopItemDelimiter != null : !LOOP_ITEM_DELIMITER_EDEFAULT.equals(loopItemDelimiter);
			case WorkflowPackage.FOREACH__LOOP_ITEM_NAME:
				return LOOP_ITEM_NAME_EDEFAULT == null ? loopItemName != null : !LOOP_ITEM_NAME_EDEFAULT.equals(loopItemName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorkflowPackage.FOREACH___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP:
				return appliesConstraint((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (loopItemDelimiter: ");
		result.append(loopItemDelimiter);
		result.append(", loopItemName: ");
		result.append(loopItemName);
		result.append(')');
		return result.toString();
	}

} //ForeachImpl
