/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.cmf.occi.core.Entity;

import org.eclipse.cmf.occi.core.impl.MixinBaseImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.pivot.evaluation.Executor;

import org.eclipse.ocl.pivot.ids.IdResolver;
import org.eclipse.ocl.pivot.ids.TypeId;

import org.eclipse.ocl.pivot.internal.utilities.PivotUtilInternal;
import org.eclipse.ocl.pivot.library.oclany.OclAnyOclIsKindOfOperation;
import org.eclipse.ocl.pivot.library.oclany.OclComparableLessThanEqualOperation;

import org.eclipse.ocl.pivot.library.string.CGStringGetSeverityOperation;
import org.eclipse.ocl.pivot.library.string.CGStringLogDiagnosticOperation;

import org.eclipse.ocl.pivot.utilities.PivotUtil;
import org.eclipse.ocl.pivot.utilities.ValueUtil;

import org.eclipse.ocl.pivot.values.IntegerValue;
import org.eclipse.ocl.pivot.values.InvalidValueException;

import workflow.Loopiteration;
import workflow.WorkflowPackage;
import workflow.WorkflowTables;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Loopiteration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link workflow.impl.LoopiterationImpl#getLoopIterationVarValue <em>Loop Iteration Var Value</em>}</li>
 *   <li>{@link workflow.impl.LoopiterationImpl#getLoopIterationVarName <em>Loop Iteration Var Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LoopiterationImpl extends MixinBaseImpl implements Loopiteration {
	/**
	 * The default value of the '{@link #getLoopIterationVarValue() <em>Loop Iteration Var Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopIterationVarValue()
	 * @generated
	 * @ordered
	 */
	protected static final String LOOP_ITERATION_VAR_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoopIterationVarValue() <em>Loop Iteration Var Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopIterationVarValue()
	 * @generated
	 * @ordered
	 */
	protected String loopIterationVarValue = LOOP_ITERATION_VAR_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoopIterationVarName() <em>Loop Iteration Var Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopIterationVarName()
	 * @generated
	 * @ordered
	 */
	protected static final String LOOP_ITERATION_VAR_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoopIterationVarName() <em>Loop Iteration Var Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopIterationVarName()
	 * @generated
	 * @ordered
	 */
	protected String loopIterationVarName = LOOP_ITERATION_VAR_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoopiterationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowPackage.Literals.LOOPITERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLoopIterationVarValue() {
		return loopIterationVarValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoopIterationVarValue(String newLoopIterationVarValue) {
		String oldLoopIterationVarValue = loopIterationVarValue;
		loopIterationVarValue = newLoopIterationVarValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.LOOPITERATION__LOOP_ITERATION_VAR_VALUE, oldLoopIterationVarValue, loopIterationVarValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLoopIterationVarName() {
		return loopIterationVarName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoopIterationVarName(String newLoopIterationVarName) {
		String oldLoopIterationVarName = loopIterationVarName;
		loopIterationVarName = newLoopIterationVarName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.LOOPITERATION__LOOP_ITERATION_VAR_NAME, oldLoopIterationVarName, loopIterationVarName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean appliesConstraint(final DiagnosticChain diagnostics, final Map<Object, Object> context) {
		/**
		 *
		 * inv appliesConstraint:
		 *   let severity : Integer[1] = 'Loopiteration::appliesConstraint'.getSeverity()
		 *   in
		 *     if severity <= 0
		 *     then true
		 *     else
		 *       let result : Boolean[1] = self.entity.oclIsKindOf(Task)
		 *       in
		 *         'Loopiteration::appliesConstraint'.logDiagnostic(self, null, diagnostics, context, null, severity, result, 0)
		 *     endif
		 */
		final /*@NonInvalid*/ Executor executor = PivotUtilInternal.getExecutor(this);
		final /*@NonInvalid*/ IdResolver idResolver = executor.getIdResolver();
		final /*@NonInvalid*/ IntegerValue severity_0 = CGStringGetSeverityOperation.INSTANCE.evaluate(executor, WorkflowTables.STR_Loopiteration_c_c_appliesConstraint);
		final /*@NonInvalid*/ boolean le = OclComparableLessThanEqualOperation.INSTANCE.evaluate(executor, severity_0, WorkflowTables.INT_0).booleanValue();
		/*@NonInvalid*/ boolean symbol_0;
		if (le) {
			symbol_0 = ValueUtil.TRUE_VALUE;
		}
		else {
			final /*@NonInvalid*/ org.eclipse.ocl.pivot.Class TYP_workflow_c_c_Task_0 = idResolver.getClass(WorkflowTables.CLSSid_Task, null);
			final /*@NonInvalid*/ Entity entity = this.getEntity();
			final /*@NonInvalid*/ boolean result = OclAnyOclIsKindOfOperation.INSTANCE.evaluate(executor, entity, TYP_workflow_c_c_Task_0).booleanValue();
			final /*@NonInvalid*/ boolean logDiagnostic = CGStringLogDiagnosticOperation.INSTANCE.evaluate(executor, TypeId.BOOLEAN, WorkflowTables.STR_Loopiteration_c_c_appliesConstraint, this, (Object)null, diagnostics, context, (Object)null, severity_0, result, WorkflowTables.INT_0).booleanValue();
			symbol_0 = logDiagnostic;
		}
		return Boolean.TRUE == symbol_0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowPackage.LOOPITERATION__LOOP_ITERATION_VAR_VALUE:
				return getLoopIterationVarValue();
			case WorkflowPackage.LOOPITERATION__LOOP_ITERATION_VAR_NAME:
				return getLoopIterationVarName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowPackage.LOOPITERATION__LOOP_ITERATION_VAR_VALUE:
				setLoopIterationVarValue((String)newValue);
				return;
			case WorkflowPackage.LOOPITERATION__LOOP_ITERATION_VAR_NAME:
				setLoopIterationVarName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowPackage.LOOPITERATION__LOOP_ITERATION_VAR_VALUE:
				setLoopIterationVarValue(LOOP_ITERATION_VAR_VALUE_EDEFAULT);
				return;
			case WorkflowPackage.LOOPITERATION__LOOP_ITERATION_VAR_NAME:
				setLoopIterationVarName(LOOP_ITERATION_VAR_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowPackage.LOOPITERATION__LOOP_ITERATION_VAR_VALUE:
				return LOOP_ITERATION_VAR_VALUE_EDEFAULT == null ? loopIterationVarValue != null : !LOOP_ITERATION_VAR_VALUE_EDEFAULT.equals(loopIterationVarValue);
			case WorkflowPackage.LOOPITERATION__LOOP_ITERATION_VAR_NAME:
				return LOOP_ITERATION_VAR_NAME_EDEFAULT == null ? loopIterationVarName != null : !LOOP_ITERATION_VAR_NAME_EDEFAULT.equals(loopIterationVarName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorkflowPackage.LOOPITERATION___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP:
				return appliesConstraint((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (loopIterationVarValue: ");
		result.append(loopIterationVarValue);
		result.append(", loopIterationVarName: ");
		result.append(loopIterationVarName);
		result.append(')');
		return result.toString();
	}

} //LoopiterationImpl
