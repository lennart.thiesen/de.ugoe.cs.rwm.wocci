/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Datalink</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * DataLink
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link workflow.Datalink#getTaskDatalinkSourceFile <em>Task Datalink Source File</em>}</li>
 *   <li>{@link workflow.Datalink#getTaskDatalinkTargetFile <em>Task Datalink Target File</em>}</li>
 *   <li>{@link workflow.Datalink#getTaskDatalinkState <em>Task Datalink State</em>}</li>
 *   <li>{@link workflow.Datalink#getTaskDatalinkStateMessage <em>Task Datalink State Message</em>}</li>
 * </ul>
 *
 * @see workflow.WorkflowPackage#getDatalink()
 * @model
 * @generated
 */
public interface Datalink extends Taskdependency {
	/**
	 * Returns the value of the '<em><b>Task Datalink Source File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Task Datalink Source File</em>' attribute.
	 * @see #setTaskDatalinkSourceFile(String)
	 * @see workflow.WorkflowPackage#getDatalink_TaskDatalinkSourceFile()
	 * @model dataType="workflow.Path" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!taskDatalinkSourceFile'"
	 * @generated
	 */
	String getTaskDatalinkSourceFile();

	/**
	 * Sets the value of the '{@link workflow.Datalink#getTaskDatalinkSourceFile <em>Task Datalink Source File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Datalink Source File</em>' attribute.
	 * @see #getTaskDatalinkSourceFile()
	 * @generated
	 */
	void setTaskDatalinkSourceFile(String value);

	/**
	 * Returns the value of the '<em><b>Task Datalink Target File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Task Datalink Target File</em>' attribute.
	 * @see #setTaskDatalinkTargetFile(String)
	 * @see workflow.WorkflowPackage#getDatalink_TaskDatalinkTargetFile()
	 * @model dataType="workflow.Path" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!taskDatalinkTargetFile'"
	 * @generated
	 */
	String getTaskDatalinkTargetFile();

	/**
	 * Sets the value of the '{@link workflow.Datalink#getTaskDatalinkTargetFile <em>Task Datalink Target File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Datalink Target File</em>' attribute.
	 * @see #getTaskDatalinkTargetFile()
	 * @generated
	 */
	void setTaskDatalinkTargetFile(String value);

	/**
	 * Returns the value of the '<em><b>Task Datalink State</b></em>' attribute.
	 * The literals are from the enumeration {@link workflow.Status}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Task Datalink State</em>' attribute.
	 * @see workflow.Status
	 * @see #setTaskDatalinkState(Status)
	 * @see workflow.WorkflowPackage#getDatalink_TaskDatalinkState()
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!taskDatalinkState'"
	 * @generated
	 */
	Status getTaskDatalinkState();

	/**
	 * Sets the value of the '{@link workflow.Datalink#getTaskDatalinkState <em>Task Datalink State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Datalink State</em>' attribute.
	 * @see workflow.Status
	 * @see #getTaskDatalinkState()
	 * @generated
	 */
	void setTaskDatalinkState(Status value);

	/**
	 * Returns the value of the '<em><b>Task Datalink State Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Human-readable explanation of the current instance state.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Task Datalink State Message</em>' attribute.
	 * @see #setTaskDatalinkStateMessage(String)
	 * @see workflow.WorkflowPackage#getDatalink_TaskDatalinkStateMessage()
	 * @model dataType="workflow.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!taskDatalinkStateMessage'"
	 * @generated
	 */
	String getTaskDatalinkStateMessage();

	/**
	 * Sets the value of the '{@link workflow.Datalink#getTaskDatalinkStateMessage <em>Task Datalink State Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Datalink State Message</em>' attribute.
	 * @see #getTaskDatalinkStateMessage()
	 * @generated
	 */
	void setTaskDatalinkStateMessage(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * start
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!start()'"
	 * @generated
	 */
	void start();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * stop
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!stop()'"
	 * @generated
	 */
	void stop();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * schedule
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!schedule()'"
	 * @generated
	 */
	void schedule();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * skip
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Datalink!skip()'"
	 * @generated
	 */
	void skip();

} // Datalink
