/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow;

import org.eclipse.cmf.occi.core.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Task
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link workflow.Task#getWorkflowTaskState <em>Workflow Task State</em>}</li>
 *   <li>{@link workflow.Task#getWorkflowTaskStateMessage <em>Workflow Task State Message</em>}</li>
 * </ul>
 *
 * @see workflow.WorkflowPackage#getTask()
 * @model
 * @generated
 */
public interface Task extends Resource {
	/**
	 * Returns the value of the '<em><b>Workflow Task State</b></em>' attribute.
	 * The literals are from the enumeration {@link workflow.Status}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Workflow Task State</em>' attribute.
	 * @see workflow.Status
	 * @see #setWorkflowTaskState(Status)
	 * @see workflow.WorkflowPackage#getTask_WorkflowTaskState()
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Task!workflowTaskState'"
	 * @generated
	 */
	Status getWorkflowTaskState();

	/**
	 * Sets the value of the '{@link workflow.Task#getWorkflowTaskState <em>Workflow Task State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Workflow Task State</em>' attribute.
	 * @see workflow.Status
	 * @see #getWorkflowTaskState()
	 * @generated
	 */
	void setWorkflowTaskState(Status value);

	/**
	 * Returns the value of the '<em><b>Workflow Task State Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Human-readable explanation of the current instance state.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Workflow Task State Message</em>' attribute.
	 * @see #setWorkflowTaskStateMessage(String)
	 * @see workflow.WorkflowPackage#getTask_WorkflowTaskStateMessage()
	 * @model dataType="workflow.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Task!workflowTaskStateMessage'"
	 * @generated
	 */
	String getWorkflowTaskStateMessage();

	/**
	 * Sets the value of the '{@link workflow.Task#getWorkflowTaskStateMessage <em>Workflow Task State Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Workflow Task State Message</em>' attribute.
	 * @see #getWorkflowTaskStateMessage()
	 * @generated
	 */
	void setWorkflowTaskStateMessage(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * start
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Task!start()'"
	 * @generated
	 */
	void start();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * schedule
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Task!schedule()'"
	 * @generated
	 */
	void schedule();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Task!stop()'"
	 * @generated
	 */
	void stop();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Skip
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Task!skip()'"
	 * @generated
	 */
	void skip();

} // Task
